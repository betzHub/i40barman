#include <recepture/ReceptureManager.h>
#include <recepture/ReceptureStateMachine.h>
#include <recepture/states/FindAndReserve.h>
#include <RFID/RFIDService.h>
#include <RFID/RFIDMock.h>

#include <opcua/UaClientService.h>
#include <opcua/UaClientMock.h>

bool running = true;

static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Received ctrl-c");
    running = false;
}

int main() {
    std::cout << "Hello, from product project!" << std::endl;

    RFIDMock rfid;
    // RFID service will wait for data an then it will create recepture manager
    UaClientService* aa = new UaClientMock();
    ReceptureStateMachine machine(*aa, rfid.GetRecepture());
    ReceptureManager manager(machine);
    manager.Process(running);

    return 0;
}