//
// Created by magino on 29. 8. 2019.
//

#include "recepture/states/WaitInQueue.h"
#include "recepture/states/FindAndReserve.h"
#include "recepture/states/FindTransport.h"

void FindAndReserve::DoAction() {
    std::cout << NAME << std::endl;
    Task task = _stateMachine.GetActualTask();
    UaClientService& client = _stateMachine.GetUaClient();

    ApplicationDescriptionArray apps;
    bool status = client.FindServersByUri(task.provider, apps);
    //if(!status) // TODO handle error

    // TODO find the best server for do the action
    /*for (unsigned i = 0; i < apps.length(); i++) {
    }*/

    // make reservation
    task.isReserved = true; // TODO on client done update RFID back to the chip

    _stateMachine.ChangeState(new WaitInQueue(_stateMachine));
}

