//
// Created by magino on 9. 9. 2019.
//
#include <iostream>

#include "recepture/states/RunTask.h"
#include "recepture/states/InitState.h"


void RunTask::DoAction() {
    std::cout << NAME << std::endl;

    Task task = _stateMachine.GetActualTask();
    UaClientService& client = _stateMachine.GetUaClient();

    //client.RunAction(task.);
    //while(client.Subscribe("ActionDone")) {
    //  usleep(1000);
    //}
    _stateMachine.IncrementTask();
    _stateMachine.ChangeState(new InitState(_stateMachine));
}