//
// Created by magino on 27. 8. 2019.
//

#include "recepture/ReceptureStateMachine.h"
#include "recepture/states/IState.h"
#include "recepture/states/InitState.h"

ReceptureStateMachine::ReceptureStateMachine(UaClientService& client, Recepture recepture):
            _client(client),
            _recepture(recepture),
            _currentState(new InitState(*this)) {}

void ReceptureStateMachine::ChangeState(IState* newState) {
    _currentState = newState;
}

void ReceptureStateMachine::DoAction() {
    _currentState->DoAction();
}

Task ReceptureStateMachine::GetActualTask() {
    // TODO if there is no more tasks return null
    return _recepture.GetActualTask();
}
