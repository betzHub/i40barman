//
// Created by magino on 28. 8. 2019.
//

#ifndef RFIDMOCK_H
#define RFIDMOCK_H

#include <vector>
#include <recepture/Task.h>

class RFIDMock: public RFIDService {
public:
    Recepture GetRecepture() {
        std::vector<Task> actions;

        Task A("A", "CoCa Cola", 20);
        actions.push_back(A);

        Task B("A", "Vodka", 40);
        actions.push_back(B);

        Task C("B", "Shake", 5);
        actions.push_back(C);

        Recepture a(actions);
        return a;
    }

    char* GetReceptureRaw() {
        return nullptr;
    }
};
#endif //RFIDMOCK_H
