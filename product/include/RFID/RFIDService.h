//
// Created by magino on 28. 8. 2019.
//

#ifndef AAS_RFIDSERVICE_H
#define AAS_RFIDSERVICE_H

#include "recepture/Recepture.h"

class RFIDService {
public:
    virtual Recepture GetRecepture() = 0;
    virtual char* GetReceptureRaw() = 0;
};
#endif //AAS_RFIDSERVICE_H
