//
// Created by magino on 28. 8. 2019.
//

#ifndef RECEPTUREMANAGER_H
#define RECEPTUREMANAGER_H

#include <thread>

#include "recepture/ReceptureStateMachine.h"
#include "recepture/Recepture.h"


class ReceptureManager {
private:
    ReceptureStateMachine& _stateMachine;
    std::thread _thread;
    bool _running;

public:
    ReceptureManager(ReceptureStateMachine& sm): _stateMachine(sm), _running(false) {}
    virtual ~ReceptureManager() {}

    void ProcessParallel() {
        _thread = std::thread(
            [this] {
                _running = true;

                while (_running) {
                    _stateMachine.DoAction();
                    usleep(500000);
                }
            }
        );
    }

    void Process(bool run) {
        while (run) {
            _stateMachine.DoAction();
            usleep(500000);
        }
    }

    void Stop() {
        _running = false;
        _thread.join();
    }
};

#endif //RECEPTUREMANAGER_H
