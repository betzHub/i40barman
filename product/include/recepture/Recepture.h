//
// Created by magino on 28. 8. 2019.
//

#ifndef RECEPTURE_H
#define RECEPTURE_H

#include <vector>
#include "recepture/Task.h"

class Recepture {
public:
    std::string uuid;
    std::string name;
    std::vector<Task> tasks;
    int currentAction;


    Recepture(std::vector<Task> tasks): tasks(tasks), currentAction(0) {}

    Task GetActualTask() {
        if(currentAction <= tasks.size())
            return tasks.at(currentAction);
        else {
            Task end;
            return end;
        }
    }
};

#endif //RECEPTURE_H
