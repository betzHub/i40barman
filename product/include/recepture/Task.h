//
// Created by magino on 28. 8. 2019.
//

#ifndef TASK_H
#define TASK_H

#include <iostream>

enum TaskState {
    Waiting,
    Reserves,
    IsReserved,
    InProgress,
    Done
};

class Task {
public:
    // app uri of production cell type
    std::string provider; // typ operace
    // operation uuid
    std::string task;
    // vector of parameters
    unsigned amount;

    // quality identifier
    // start / end time stamp or time of work

    // TODO state
    TaskState state;
    // this task is already reserved in production cell
    bool isReserved;
    std::string reservedProductionCell;
    std::string reserverProductionCellUuid;
    // maybe store whole endpoint of reserved action

    //error status
    Task() {}
    Task(std::string provider, std::string task, unsigned amount): provider(provider), task(task), amount(amount) {}
};

#endif //TASK_H
