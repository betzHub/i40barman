//
// Created by magino on 11. 8. 2019.
//

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include "opcua/UaClientService.h"
#include "recepture/Recepture.h"
#include "recepture/Task.h"

class IState;

class ReceptureStateMachine {
private:
    UaClientService& _client;
    Recepture& _recepture;
    IState* _currentState;
public:
    ReceptureStateMachine(UaClientService& client, Recepture recepture);
    virtual ~ReceptureStateMachine() = default;

    void ChangeState(IState* newState);
    void DoAction();
    Task GetActualTask();
    void IncrementTask() {
        _recepture.currentAction++;
    }
    UaClientService& GetUaClient() { return _client; }
};
#endif //STATEMACHINE_H
