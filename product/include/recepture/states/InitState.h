//
// Created by magino on 4. 9. 2019.
//

#ifndef AAS_INITSTATE_H
#define AAS_INITSTATE_H

#include "recepture/states/IState.h"
#include "recepture/states/FindAndReserve.h"
#include "recepture/states/CheckPosition.h"

class ReceptureStateMachine;

class InitState: public IState {
private:
    const std::string NAME = "Init state";
public:
    InitState(ReceptureStateMachine& sm): IState(sm) {}
    ~InitState() {};

    void DoAction() {
        std::cout << NAME << std::endl;
        Task task = _stateMachine.GetActualTask();
        // If there is no more tasks, task is null and i should transport glass to end

        switch (task.state) {
            case TaskState::IsReserved: {
                _stateMachine.ChangeState(new FindAndReserve(_stateMachine));
                return;
            }
        }

        if(!task.isReserved) {
            _stateMachine.ChangeState(new FindAndReserve(_stateMachine));
            return;
        } else {
            _stateMachine.ChangeState(new CheckPosition(_stateMachine));
            return;
        }
    }

};
#endif //AAS_INITSTATE_H
