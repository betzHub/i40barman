//
// Created by magino on 4. 9. 2019.
//

#ifndef AAS_CHECKPOSITION_H
#define AAS_CHECKPOSITION_H

#include "recepture/states/IState.h"
#include "recepture/states/FindTransport.h"
#include "recepture/states/RunTask.h"

class ReceptureStateMachine;

class CheckPosition: public IState {
private:
    const std::string NAME = "Check Position";
public:
    CheckPosition(ReceptureStateMachine& sm): IState(sm) {}
    ~CheckPosition() {};

    void DoAction() {
        std::cout << NAME << std::endl;
        Task task = _stateMachine.GetActualTask();

        // find localhost ua server uuid and copare it with task reserved cell uuid
        bool isOnPosition = task.reserverProductionCellUuid == ""/*GetLocalhostServerUuid()*/;
        if(isOnPosition){
            _stateMachine.ChangeState(new RunTask(_stateMachine));
            return;
        } else {
            _stateMachine.ChangeState(new FindTransport(_stateMachine));
            return;
        }
    }

};

#endif //AAS_CHECKPOSITION_H
