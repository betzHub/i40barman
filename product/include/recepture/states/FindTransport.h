//
// Created by magino on 14. 8. 2019.
//

#ifndef FINDTRANSPORT_H
#define FINDTRANSPORT_H

#include "recepture/states/IState.h"
class ReceptureStateMachine;

class FindTransport: public IState {
private:
    const std::string NAME = "Find Transport";
public:
    FindTransport(ReceptureStateMachine& sm): IState(sm) {}

    void DoAction() {
        std::cout << NAME << std::endl;
        Task task = _stateMachine.GetActualTask();
        // wait until the cell will have free spot
        // subscription + callback

        // call transport
        // In this point whole recepture must be loaded inside RFID because the glass will be moved to anoither station
        // TODO END state machine
    }
};

#endif //FINDTRANSPORT_H
