//
// Created by magino on 11. 8. 2019.
//

#ifndef ISTATE_H
#define ISTATE_H

#include <client/UaClient.h>
#include "recepture/ReceptureStateMachine.h"

class IState {
protected:
    ReceptureStateMachine& _stateMachine;
public:
    IState(ReceptureStateMachine& sm): _stateMachine(sm) {}
    virtual ~IState() {}
    virtual void DoAction() = 0;
};

#endif //ISTATE_H
