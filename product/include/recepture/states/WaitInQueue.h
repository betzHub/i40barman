//
// Created by magino on 4. 9. 2019.
//

#ifndef AAS_WAITINQUEUE_H
#define AAS_WAITINQUEUE_H

#include "recepture/states/IState.h"
#include "recepture/states/FindTransport.h"
#include "recepture/states/RunTask.h"

class ReceptureStateMachine;

class WaitInQueue: public IState {
private:
    const std::string NAME = "Wait In Queue";
public:
    WaitInQueue(ReceptureStateMachine& sm): IState(sm) {}
    ~WaitInQueue() {};

    void DoAction() {
        std::cout << NAME << std::endl;
        Task task = _stateMachine.GetActualTask();
        UaClientService& client = _stateMachine.GetUaClient();
        // wait until is your turn then change state
        // create subscription and callback will change state
        _stateMachine.ChangeState(new FindTransport(_stateMachine));
    }

};

#endif //AAS_WAITINQUEUE_H
