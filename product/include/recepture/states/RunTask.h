//
// Created by magino on 14. 8. 2019.
//

#ifndef RUNTASK_H
#define RUNTASK_H

#include "recepture/states/IState.h"

class ReceptureStateMachine;

class RunTask: public IState {
private:
    const std::string NAME = "Run Task";
public:
    RunTask(ReceptureStateMachine& sm): IState(sm) {}
    ~RunTask() {}

    virtual void DoAction();
};

#endif //RUNTASK_H
