//
// Created by magino on 11. 8. 2019.
//

#ifndef LOADTASK_H
#define LOADTASK_H

#include "recepture/states/IState.h"

class ReceptureStateMachine;

class FindAndReserve: public IState {
private:
    const std::string NAME = "Find And Reserve";
public:
    FindAndReserve(ReceptureStateMachine& sm): IState(sm) {}

    void DoAction();
};
#endif //LOADTASK_H
