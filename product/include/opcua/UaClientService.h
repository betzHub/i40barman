//
// Created by magino on 31. 8. 2019.
//

#ifndef OPCUASERVICE_H
#define OPCUASERVICE_H

#include <iostream>
#include <core/UaObjects.h>

class UaClientService {
public:
    virtual bool FindServersByUri(const std::string& serverUri, ApplicationDescriptionArray& registeredServers) = 0;

};
#endif //OPCUASERVICE_H
