//
// Created by magino on 31. 8. 2019.
//

#ifndef OPCUAMOCK_H
#define OPCUAMOCK_H

#include "opcua/UaClientService.h"
#include "open62541/types_generated.h"
#include "open62541/types.h"

class UaClientMock: public UaClientService {
public:
    UaClientMock() {}
    ~UaClientMock() {}
    bool FindServersByUri(const std::string& serverUri,
                          ApplicationDescriptionArray& registeredServers) {
        UA_ApplicationDescription desc;
        desc.applicationType = UA_APPLICATIONTYPE_DISCOVERYSERVER;
        desc.productUri = UA_String_fromChars("storage:A");
        desc.applicationUri = UA_String_fromChars("vut:storage:A");
        desc.applicationName = UA_LOCALIZEDTEXT((char*)"en-UA",(char*)"A");
        desc.discoveryUrlsSize = 1;
        UA_String url = UA_String_fromChars("opc.tcp://marek-ubuntu18:4840");
        desc.discoveryUrls = &url;
        registeredServers.setList(1, &desc);
        return UA_STATUSCODE_GOOD;
    }
};

#endif //OPCUAMOCK_H
