//
// Created by magino on 11.4.19.
//

#include <server/UaServer.h>
#include <open62541/server_config_default.h>
#include <open62541/client.h>

UaClient* UaServer::GetRegisterClient() {
    if(!_registerClient) {
        _registerClient = new UaClient();
    }
    return _registerClient;
}

UaServer::UaServer() {
    _server = UA_Server_new();
    UA_ServerConfig* _config = UA_Server_getConfig(_server);
    UA_ServerConfig_setDefault(_config);
}
UaServer::UaServer(unsigned short port,
                   UA_ByteString* certificate,
                   UA_ApplicationDescription appDescription,
                   UA_MdnsDiscoveryConfiguration mdnsCfg) {
    _server = UA_Server_new();
    UA_ServerConfig* _config = UA_Server_getConfig(_server);
    UA_StatusCode status = UA_ServerConfig_setMinimal(_config, port, certificate);
    /*if(appDescription) {
        UA_ApplicationDescription_clear(&_config->applicationDescription);
        UA_StatusCode a = UA_ApplicationDescription_copy(appDescription, &_config->applicationDescription);
    }
    if(mdnsCfg) {
        _config->discovery.mdnsEnable = true;
        UA_StatusCode b = UA_MdnsDiscoveryConfiguration_copy(mdnsCfg, &_config->discovery.mdns);
        //_config->discovery.mdns = *mdnsCfg; TODO preco to takto nemozem priradit?
    }*/

    UA_ApplicationDescription_clear(&_config->applicationDescription);
    _config->applicationDescription = appDescription;
    UA_MdnsDiscoveryConfiguration_clear(&_config->discovery.mdns);
    _config->discovery.mdnsEnable = true;
    _config->discovery.mdns = mdnsCfg;
}

void UaServer::CleanUp() {
    // TODO unregister all registered servers and remove repeated callbacks
    //UA_StatusCode retVal = UA_Server_unregister_discovery(_server, _registerClient);
    /*if(retVal != UA_STATUSCODE_GOOD)
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
                     "Could not unregister server from discovery server. StatusCode %s", UA_StatusCode_name(retVal));*/
    if (_registerClient) {
        _registerClient->~UaClient();
        _registerClient = nullptr;
    }
    UA_Server_delete(_server);
    _server = nullptr;
}

bool UaServer::Run(volatile UA_Boolean *running) {

    if (UA_Server_run(_server, running) == UA_STATUSCODE_GOOD) {
        return true;
    } else return false;
}

bool UaServer::RunAsync(volatile UA_Boolean* running) {
    _thread = std::thread([this, running] () {
        UA_Server_run(_server, running);
    });
}

bool UaServer::AddPeriodicServerRegister(std::string endpointUrl, unsigned period, unsigned delay, unsigned long* periodicCallbackId) {
    UA_StatusCode status = UA_Server_addPeriodicServerRegisterCallback(_server, _registerClient->GetClient(), endpointUrl.c_str(), UA_Double(period), UA_Double(delay), periodicCallbackId);
    return status == UA_STATUSCODE_GOOD;
}

void UaServer::ServerOnNetworkCallback(const UA_ServerOnNetwork *serverOnNetwork, UA_Boolean isServerAnnounce,
                                       UA_Boolean isTxtReceived, void *data) {
    if(!isServerAnnounce) {
        UA_LOG_DEBUG(UA_Log_Stdout,
                     UA_LOGCATEGORY_SERVER,
                     "ServerOnNetworkCallback called, but discovery URL already initialized or is not announcing. Ignoring.");
        return; // We already have everything we need or we only want server announces
    }

    if(!isTxtReceived) return;
    // we wait until the corresponding TXT record is announced.
    // Problem: how to handle if a Server does not announce the optional TXT?

    UA_LOG_INFO(UA_Log_Stdout,
                UA_LOGCATEGORY_SERVER,
                "Another server announced itself on %.*s",
                (int)serverOnNetwork->discoveryUrl.length, serverOnNetwork->discoveryUrl.data);

    UaServer* server = static_cast<UaServer*>(data);
    server->ServerOnNetwork(serverOnNetwork);
}

void UaServer::RegisterServerCallback(const UA_RegisteredServer* registeredServer, void* data) {
    UaServer* server = static_cast<UaServer*>(data);
    server->ServerRegisterActivity();
}

/*
UaServer::UaServer(UaServerCfg cfg) {
    _config = UA_ServerConfig_new_minimal(cfg.port, NULL);

    //UA_ServerConfig_set_customHostname(_config, UA_String_fromChars("127.0.0.1"));
    //UA_ServerConfig_set_customHostname(_config, UA_String_fromChars("192.168.138.132"));
    UA_LocalizedText_clear(&_config->applicationDescription.applicationName);
    _config->applicationDescription.applicationName = UA_LOCALIZEDTEXT_ALLOC("en-US", cfg.appName.c_str());

    // To enable mDNS discovery, set application type to discovery server.
    _config->applicationDescription.applicationType = UA_APPLICATIONTYPE_DISCOVERYSERVER;

    UA_String_clear(&_config->applicationDescription.applicationUri);
    _config->applicationDescription.applicationUri = UA_String_fromChars(cfg.appUri.c_str());

    _config->applicationDescription.discoveryUrlsSize = 1;
    _config->applicationDescription.discoveryUrls = (UA_String *) UA_Array_new(1, &UA_TYPES[UA_TYPES_STRING]);
    _config->applicationDescription.discoveryUrls[0] = UA_String_fromChars("opc.tcp://192.168.138.132:4850");

    UA_String_clear(&_config->applicationDescription.productUri);
    _config->applicationDescription.productUri = UA_String_fromChars(cfg.productUri.c_str());

    _config->mdnsServerName = UA_String_fromChars(cfg.mDnsName.c_str());

    // See http://www.opcfoundation.org/UA/schemas/1.03/ServerCapabilities.csv
    _config->serverCapabilitiesSize = 2;
    _config->serverCapabilities = (UA_String *) UA_Array_new(2, &UA_TYPES[UA_TYPES_STRING]);
    _config->serverCapabilities[0] = UA_String_fromChars("LDS");
    _config->serverCapabilities[1] = UA_String_fromChars("DA");

    // Default value of registered server cleanup is 1hour = 60*60
    // config->discoveryCleanupTimeout = 60*60;

    // TODO setup 'AccessControl'
    _server = UA_Server_new(_config);

    //TODO load address space depending on server capabilities
}*/