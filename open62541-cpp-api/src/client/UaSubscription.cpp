//
// Created by magino on 21.7.2019.
//

#include "client/UaSubscription.h"
#include "client/UaClient.h"

UaSubscription::UaSubscription(UaClient& client) : _opcUaClient(client) {
    _request = UA_CreateSubscriptionRequest_default();

    // TODO put connection somewhere else or hold connection state inside UaSubscription
    _response = UA_Client_Subscriptions_create(_opcUaClient.GetClient(), _request, this, StatusChangeNotificationCallback, DeleteSubscriptionCallback);
    UA_StatusCode  status = _response.responseHeader.serviceResult;
}

UaSubscription::UaSubscription(UaClient& client, const int publishInterval, const int priority) : _opcUaClient(client) {
    _request = UA_CreateSubscriptionRequest_default();
    _request.requestedPublishingInterval = publishInterval;
    _request.priority = priority;

    // TODO put connection somewhere else or hold connection state inside UaSubscription
    _response = UA_Client_Subscriptions_create(_opcUaClient.GetClient(), _request, this, StatusChangeNotificationCallback, DeleteSubscriptionCallback);
    UA_StatusCode  status = _response.responseHeader.serviceResult;
}

UaSubscription::~UaSubscription() {
    // Delete all monitored items
    DeleteSubscription();
}

void UaSubscription::ModifySubscription() {
    UA_ModifySubscriptionRequest request;
    UA_ModifySubscriptionRequest_init(&request);

    UA_ModifySubscriptionResponse response = UA_Client_Subscriptions_modify(_opcUaClient.GetClient(), request);
    UA_StatusCode status = response.responseHeader.serviceResult;
}

void UaSubscription::DeleteSubscription() {
    UA_StatusCode status = UA_Client_Subscriptions_deleteSingle(_opcUaClient.GetClient(), _response.subscriptionId);
}

void UaSubscription::SetPublishingMode(bool enable) {
    UA_SetPublishingModeRequest request;
    UA_SetPublishingModeRequest_init(&request);
    request.publishingEnabled = enable;

    UA_SetPublishingModeResponse response = UA_Client_Subscriptions_setPublishingMode(_opcUaClient.GetClient(), request);
    UA_StatusCode status = response.results[0];
}

void UaSubscription::AddMonitoredItem(UaNodeId node, MonitorItemFunc func) {
    auto monItem = new MonitoredItemDataChange(*this, func);
    if(monItem->AddDataChange(node)) {
        MonitoredItemRef item(monItem);
        _monitoredItems[item->GetId()] = item;
    } else delete monItem;
}

void UaSubscription::AddMonitoredItems(std::vector<UaNodeId> nodeIds) {
    //UA_CreateMonitoredItemsRequest request;
    //UA_Client_MonitoredItems_createDataChanges(
}