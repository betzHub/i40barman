//
// Created by magino on 5. 8. 2019.
//

#ifndef UAOBJECTS_H
#define UAOBJECTS_H

#include <iostream>
#include <vector>
#include <stdexcept>
#include <open62541/types_generated.h>
#include <open62541/types_generated_handling.h>
#include <open62541/plugin/accesscontrol_default.h>

#include "UaNodeId.h"

template <typename T, const int I>
class Array {
    size_t _length = 0;
    T* _data = nullptr;
public:
    Array() {}
    Array(T* data, size_t len): _data(data), _length(len) {}
    Array(size_t n) { allocate(n); }
    ~Array() { clear(); }

    void allocate(size_t len) {
        clear();
        _data = (T *)(UA_Array_new(len, &UA_TYPES[I]));
        _length = len;
    }

    void release() {
        _length = 0;
        _data = nullptr;
    }

    void clear() {
        if (_length && _data) {
            UA_Array_delete(_data, _length, &UA_TYPES[I]);
        }
        _length = 0;
        _data = nullptr;
    }

    T& at(size_t i) const {
        if (!_data || (i >= _length)) throw std::exception();
        return _data[i];
    }

    void setList(size_t len, T *data) {
        clear();
        _length = len;
        _data = data;
    }

    size_t length() const { return _length; }
    T* data() const { return _data; }
    size_t* lengthRef() { return &_length; }
    T** dataRef() { return &_data; }
    operator T*() { return _data; }
};

class UA_EXPORT Argument {
public:
    UA_Argument argument;
    void* context;

    void setDataType(int i) {
        //get().dataType = UA_TYPES[i].typeId;
    }
    void setDescription(const std::string &s) {
        //get().description = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setName(const std::string &s) {
        //get().name = UA_STRING_ALLOC(s.c_str());
    }
    void setValueRank(int i) {
        //get().valueRank = i;   /* scalar argument */
    }
};

class UA_EXPORT ArgumentList: public std::vector<Argument> {
public:
    // use constant strings for argument names - else memory leak
    void AddScalarArgument(const char* name, int type, void* context) {
        Argument arg;
        UA_Argument a;
        UA_Argument_init(&a);
        a.description = UA_LOCALIZEDTEXT((char*)"en_US", (char*)name);
        a.name = UA_STRING((char*)name);
        a.dataType = UA_TYPES[type].typeId;
        a.valueRank = UA_VALUERANK_SCALAR;

        arg.argument = a;
        arg.context = context;

        push_back(arg);
    }

    void AddArrayArgument(const char* name, int type, int size, void* context) {
        Argument arg;
        UA_Argument a;
        UA_Argument_init(&a);
        a.description = UA_LOCALIZEDTEXT((char*)"en_US", (char*)name);
        a.name = UA_STRING((char*)name);
        a.dataType = UA_TYPES[type].typeId;
        a.valueRank = UA_VALUERANK_ONE_DIMENSION;
        a.arrayDimensionsSize = 1;
        a.arrayDimensions = new UA_UInt32(size);

        arg.argument = a;
        arg.context = context;

        push_back(arg);
    }

    std::vector<UA_Argument> GetArguments() {
        std::vector<UA_Argument> arguments;

        for(auto it = begin(); it != end(); ++it) {
            const auto& elem = *it;
            arguments.push_back(elem.argument);
        }
        return arguments;
    }
};

template<typename T>
T GetVariantValue(UA_Variant variant) {
    if (!UA_Variant_isEmpty(&variant) && variant.arrayDimensionsSize == 0) {
        //return *((T *)variant.data); // cast to a value - to do Type checking needed
        return *static_cast<T*>(variant.data);
    }
    return T();
}

// typedef basic array types
typedef Array<UA_String, UA_TYPES_STRING> StringArray;
typedef Array<UA_NodeId, UA_TYPES_NODEID> NodeIdArray;
typedef Array<UA_Variant, UA_TYPES_VARIANT> VariantArray;
typedef Array<UA_ReferenceDescription, UA_TYPES_REFERENCEDESCRIPTION> ReferenceDescriptionArray;
typedef Array<UA_ServerOnNetwork, UA_TYPES_SERVERONNETWORK> ServerOnNetworkArray;
typedef Array<UA_ApplicationDescription, UA_TYPES_APPLICATIONDESCRIPTION> ApplicationDescriptionArray;
typedef Array<UA_EndpointDescription, UA_TYPES_ENDPOINTDESCRIPTION> EndpointDescriptionArray;
typedef Array<UA_QualifiedName, UA_TYPES_QUALIFIEDNAME> QualifiedNameArray;

// non-heap allocation - no delete
inline UA_String ToUA_String(const std::string& s) {
    UA_String r;
    r.length = s.size();
    r.data = (UA_Byte*)(s.c_str());
    return r;
}

inline std::string ToStdString(UA_String& string) {
    std::string str((const char*)(string.data), string.length);
    return str;
}

inline void FromStdString(const std::string& s, UA_String& r) {
    UA_String_deleteMembers(&r);
    r = UA_STRING_ALLOC(s.c_str());
}

inline std::string StatusCodeToString(UA_StatusCode c) {
    return std::string(UA_StatusCode_name(c));
}

inline char* UaStringToPChar(const UA_String *uaString) {
    char* string = (char*)UA_malloc(uaString->length + 1);
    memcpy(string, uaString->data, uaString->length);
    string[uaString->length] = 0;
    return string;
}

inline char* stringToPChar(std::string* string) {
    char* tmp = (char*)UA_malloc(string->length() + 1);
    memcpy(tmp, string->c_str(), string->length());
    tmp[string->length()] = 0;
    return tmp;
}

inline const char* stringToConstPChar(std::string* string) {
    return string->c_str();
}
/*
std::string NodeIdtoStr(const UA_NodeId &nodeId) {
    std::string ret = std::to_string(nodeId.namespaceIndex) + ":";

    switch (nodeId.identifierType) {
        case UA_NODEIDTYPE_NUMERIC:
            return ret + std::to_string(nodeId.identifier.numeric);

        case UA_NODEIDTYPE_BYTESTRING:
        case UA_NODEIDTYPE_STRING:
            return ret + std::string((const char *)(nodeId.identifier.string.data), nodeId.identifier.string.length);
        case UA_NODEIDTYPE_GUID: {
            char b[45];
            int l = sprintf(b, "%08X:%04X:%04X[%02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X]",
                            nodeId.identifier.guid.data1,
                            nodeId.identifier.guid.data2,
                            nodeId.identifier.guid.data3,
                            nodeId.identifier.guid.data4[0],
                            nodeId.identifier.guid.data4[1],
                            nodeId.identifier.guid.data4[2],
                            nodeId.identifier.guid.data4[3],
                            nodeId.identifier.guid.data4[4],
                            nodeId.identifier.guid.data4[5],
                            nodeId.identifier.guid.data4[6],
                            nodeId.identifier.guid.data4[7]);

            return ret + std::string(b, l);
        }
        default:
            break;
    }
    return std::string("Invalid Node Type");
}*/

class UA_EXPORT UsernamePasswordLogin {
    UA_UsernamePasswordLogin lgn;
public:

    UsernamePasswordLogin(const std::string &userName="", const std::string &password="") {
        //UA_String_init(&ref()->username);
        //UA_String_init(&ref()->password);
        setUserName(userName);
        setPassword(password);
    }

    ~UsernamePasswordLogin() {
        //UA_String_deleteMembers(&ref()->username);
        //UA_String_deleteMembers(&ref()->password);
    }

    void setUserName(const std::string &s) {
        //fromStdString(s, ref()->username);
    }

    void setPassword(const std::string &s) {
        //fromStdString(s, ref()->password);
    }
};

class UA_EXPORT ObjectAttributes/* : public TypeBase<UA_ObjectAttributes> */{
    UA_ObjectAttributes attr;

public:
    void setDefault() {
        attr = UA_ObjectAttributes_default;
    }

    void setDisplayName(const std::string &s) {
        //get().displayName = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }

    void setDescription(const std::string &s) {
        //get().description = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }

    void setSpecifiedAttributes(UA_UInt32 m) {
        //get().specifiedAttributes = m;
    }

    void setWriteMask(UA_UInt32 m) {
        //get().writeMask = m;
    }

    void setUserWriteMask(UA_UInt32 m) {
        //get().userWriteMask = m;
    }

    void setEventNotifier(unsigned m) {
        //get().eventNotifier = (UA_Byte)m;
    }
};

class UA_EXPORT ObjectTypeAttributes/*: public TypeBase<UA_ObjectTypeAttributes>*/ {
    UA_ObjectTypeAttributes attr;
public:
    //UA_TYPE_DEF(ObjectTypeAttributes)
    void setDefault() {
        attr = UA_ObjectTypeAttributes_default;
    }

    void setDisplayName(const std::string &s) {
        //get().displayName = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setDescription(const std::string &s) {
        //get().description = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setSpecifiedAttributes(UA_UInt32 m) {
        //get().specifiedAttributes = m;
    }
    void setWriteMask(UA_UInt32 m) {
        //get().writeMask = m;
    }
    void setUserWriteMask(UA_UInt32 m) {
        //get().userWriteMask = m;
    }
    void setIsAbstract(bool f) {
        //get().isAbstract = f;
    }
};

class UA_EXPORT VariableAttributes/* : public TypeBase<UA_VariableAttributes>*/{
    UA_VariableAttributes attr;
public:
    //UA_TYPE_DEF(VariableAttributes)
    void setDefault() {
        attr = UA_VariableAttributes_default;
    }

    void setDisplayName(const std::string &s) {
        //get().displayName = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setDescription(const std::string &s) {
        //get().description = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    //void setValue(Variant &v) {
        //UA_Variant_copy(v,  &get().value); // deep copy the variant - do not know life times
    //}
    void setValueRank(int i) {
        //get().valueRank = i;
    }
};

class UA_EXPORT VariableTypeAttributes/* : public TypeBase<UA_VariableTypeAttributes>*/{
    UA_VariableTypeAttributes attr;
public:
    //UA_TYPE_DEF(VariableTypeAttributes)
    void setDefault() {
        attr = UA_VariableTypeAttributes_default;
    }
    void setDisplayName(const std::string &s) {
        //get().displayName = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setDescription(const std::string &s) {
        //get().description = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
};

class UA_EXPORT ViewAttributes/*: public TypeBase<UA_ViewAttributes>*/ {
public:
    //UA_TYPE_DEF(ViewAttributes)
    void setDefault() {
        //*this = UA_ViewAttributes_default;
    }
};

class UA_EXPORT ReferenceTypeAttributes/* : public TypeBase< UA_ReferenceTypeAttributes>*/{
public:
    //UA_TYPE_DEF(ReferenceTypeAttributes)
    void setDefault() {
        //*this = UA_ReferenceTypeAttributes_default;
    }
};

class UA_EXPORT DataTypeAttributes/* : public TypeBase<UA_DataTypeAttributes>*/{
public:
    //UA_TYPE_DEF(DataTypeAttributes)
    void setDefault() {
        //*this = UA_DataTypeAttributes_default;
    }
};

class UA_EXPORT UaExpandedNodeId/*: public TypeBase<UA_ExpandedNodeId>*/{
private:
    UA_ExpandedNodeId expandedNodeId;
public:
    //UA_TYPE_DEF(ExpandedNodeId)
    static UaExpandedNodeId ModellingRuleMandatory;
    UaExpandedNodeId(){
        expandedNodeId = UA_EXPANDEDNODEID_NULL;
    }
    UaExpandedNodeId(int nsIndex, int identifier) {
        expandedNodeId = UA_EXPANDEDNODEID_NUMERIC(nsIndex, identifier);
    }

    UaExpandedNodeId(UA_ExpandedNodeId expandedNodeId) {
        expandedNodeId = expandedNodeId;
    }

    UaExpandedNodeId(const std::string namespaceUri, UA_NodeId &nodeId, int serverIndex) /*: TypeBase(UA_ExpandedNodeId_new()) */{
        expandedNodeId.nodeId = nodeId;
        expandedNodeId.namespaceUri = ToUA_String(namespaceUri);
        expandedNodeId.serverIndex = serverIndex;
    }

    ~UaExpandedNodeId(){}

    UA_NodeId& GetNodeId() { return expandedNodeId.nodeId; }
    UA_String& GetNamespaceUri() { return expandedNodeId.namespaceUri; }
    UA_UInt32 GetServerIndex() { return expandedNodeId.serverIndex; }
};

class UA_EXPORT MethodAttributes/* : public TypeBase<UA_MethodAttributes>*/{
    UA_MethodAttributes attr;
public:
    //UA_TYPE_DEF(MethodAttributes)
    void setDefault() {
        attr = UA_MethodAttributes_default;
    }
    void setDisplayName(const std::string &s) {
        //get().displayName = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setDescription(const std::string &s) {
        //get().description = UA_LOCALIZEDTEXT_ALLOC("en_US", s.c_str());
    }
    void setExecutable(bool exe = true, bool user = true) {
        //get().executable = exe;
        //get().userExecutable = user;
    }
};

class UA_EXPORT QualifiedName/*:public TypeBase<UA_QualifiedName>*/{
public:
    //UA_TYPE_DEF(QualifiedName)
    QualifiedName(int ns, const char *s)/*:TypeBase(UA_QualifiedName_new())*/{
        //*(_d.get()) = UA_QUALIFIEDNAME_ALLOC(ns, s);
    }
    QualifiedName(int ns, const std::string &s)/*:TypeBase(UA_QualifiedName_new())*/{
        //*(_d.get()) = UA_QUALIFIEDNAME_ALLOC(ns, s.c_str());
    }
    //UA_UInt16 namespaceIndex() { return ref()->namespaceIndex;}
    //UA_String &name() { return ref()->name;}
};

/*class UA_EXPORT LocalizedText: public TypeBase<UA_LocalizedText>{
public:
    //UA_TYPE_DEF(LocalizedText)
    LocalizedText(const std::string &locale, const std::string &text) : TypeBase(UA_LocalizedText_new()){
        //get() = UA_LOCALIZEDTEXT_ALLOC(locale.c_str(), text.c_str());
    }
};*/
/*
class RelativePathElement: {
public:
    //UA_TYPE_DEF(RelativePathElement)
    RelativePathElement(QualifiedName &item, UaNodeId &typeId, bool inverse = false, bool includeSubTypes = false):
            TypeBase(UA_RelativePathElement_new()){
        //get().referenceTypeId = typeId.get();
        //get().isInverse = includeSubTypes;
        //get().includeSubtypes = inverse;
        //get().targetName = item.get(); // shallow copy!!!
    }
};*/

class UA_EXPORT RelativePath/* : public TypeBase<UA_RelativePath>*/{
public:
    //UA_TYPE_DEF(RelativePath)
};

/*class UA_EXPORT BrowsePath: public TypeBase<UA_BrowsePath>{
public:
    //UA_TYPE_DEF(BrowsePath)
    BrowsePath(UaNodeId &start, RelativePath &path) : TypeBase(UA_BrowsePath_new()){
       // UA_RelativePath_copy(path.constRef(), &get().relativePath); // deep copy
        //UA_NodeId_copy(start, &get().startingNode);
    }
    BrowsePath(UaNodeId &start, RelativePathElement &path): TypeBase(UA_BrowsePath_new()){
        //get().startingNode = start.get();
        //get().relativePath.elementsSize = 1;
        //get().relativePath.elements = path.ref();
    }
};*/

class UA_EXPORT BrowseResult/* : public TypeBase<UA_BrowseResult>*/{
public:
    //UA_TYPE_DEF(BrowseResult)
};

class UA_EXPORT BrowsePathResult/* : public TypeBase<UA_BrowsePathResult>*/{
public:
    //UA_TYPE_DEF(BrowsePathResult)
};

class UA_EXPORT CallMethodRequest/* : public TypeBase<UA_CallMethodRequest>*/{
public:
    //UA_TYPE_DEF(CallMethodRequest)
};

class UA_EXPORT CallMethodResult/* : public TypeBase<UA_CallMethodResult>*/{
public:
    //UA_TYPE_DEF(CallMethodResult)
};

#endif //UAOBJECTS_H