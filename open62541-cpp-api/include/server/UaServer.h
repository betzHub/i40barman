//
// Created by magino on 11.4.19.
//

#ifndef UASERVER_H
#define UASERVER_H

#include <open62541/server.h>
#include <open62541/server_config.h>
#include <open62541/plugin/log_stdout.h>

#include "client/UaClient.h"
#include "core/UaObjects.h"
#include "core/UaNodeId.h"

class UaServer {
private:
    UA_Server* _server;
    // TODO initialize with constructor for register client
    UaClient* _registerClient;
    std::thread _thread;
    int defaultNamespaceIndex;

    /**
     * This callback is called if another server registeres or unregisters with this instance.
     */
    static void RegisterServerCallback(const UA_RegisteredServer *registeredServer, void* data);

    /**
     * This callback runs when LDS ME server announce itself on network.
     */
    static void ServerOnNetworkCallback(const UA_ServerOnNetwork *serverOnNetwork, UA_Boolean isServerAnnounce, UA_Boolean isTxtReceived, void *data);
public:
    UA_Server* GetServer() { return _server; }
    UaClient* GetRegisterClient();

    /**
    * Server Lifecycle
    * ---------------- */
    UaServer();
    UaServer(unsigned short port,
             UA_ByteString* certificate,
             UA_ApplicationDescription appDescription,
             UA_MdnsDiscoveryConfiguration mdnsCfg);
    virtual ~UaServer() { CleanUp(); }

    void CleanUp(); // TODO do i need destructor logic in separate clean up method?

    void SetServerCallbacks() {
        // Callback which is called when a new server is detected through mDNS, needs to be set after UA_Server_run_startup or UA_Server_run
        UA_Server_setServerOnNetworkCallback(_server, ServerOnNetworkCallback, this);
        UA_Server_setRegisterServerCallback(_server, RegisterServerCallback, this);
    }

    bool Run(volatile UA_Boolean* running);
    bool RunAsync(volatile UA_Boolean* running);
    // UA_Server_run_startup
    // UA_Server_run_iterate
    // UA_Server_run_shutdown

    /**
     * Timed Callbacks
     * --------------- */
    // -> callback UA_ServerCallback(UA_Server *server, void *data)
    //UA_Server_addTimedCallback(UA_Server *server, UA_ServerCallback callback, void *data, UA_DateTime date, UA_UInt64 *callbackId); // Runs just once
    //UA_Server_addRepeatedCallback(UA_Server *server, UA_ServerCallback callback, void *data, UA_Double interval_ms, UA_UInt64 *callbackId);
    //UA_Server_changeRepeatedCallbackInterval(UA_Server *server, UA_UInt64 callbackId, UA_Double interval_ms);

    //UA_Server_removeCallback(UA_Server *server, UA_UInt64 callbackId); // Remove a repeated callback. Does nothing if the callback is not found.

    /**
     * Reading and Writing Node Attributes
     * ----------------------------------- */
     // ReadBrowseName()
    std::string ReadBrowseName(UaNodeId nodeId) {
        UA_QualifiedName qualifiedName;
        UA_QualifiedName_init(&qualifiedName);
        UA_Server_readBrowseName(_server, nodeId.GetNodeId(), &qualifiedName);
        return ToStdString(qualifiedName.name);
        //return qualifiedName;
    }


    /**
     * Browsing
     * -------- */
    bool Browse(UA_BrowseDescription* desc, ReferenceDescriptionArray& references, unsigned maxReferences = 0) {
        UA_BrowseResult result = UA_Server_browse(_server, maxReferences, desc);
        if(result.statusCode != UA_STATUSCODE_GOOD) return false;
        references.setList(result.referencesSize, result.references);
        // result.continuationPoint TODO what is continuationPoint?
        return true;
    }
    /**
     *
     * @param references
     * @param maxReferences The value 0 indicates that the Client is imposing no limitation.
     * @return
     */
    bool BrowseRoot(ReferenceDescriptionArray& references, unsigned maxReferences = 0) {
        UA_BrowseDescription* desc = UA_BrowseDescription_new();
        desc->nodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ROOTFOLDER);
        desc->browseDirection = UA_BROWSEDIRECTION_FORWARD;
        desc->nodeClassMask = UA_NODECLASS_OBJECT | UA_NODECLASS_METHOD | UA_NODECLASS_VARIABLE;
        desc->includeSubtypes = true;
        desc->referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HIERARCHICALREFERENCES);
        desc->resultMask = UA_BROWSERESULTMASK_ALL;

        return Browse(desc, references, maxReferences);
    }

    bool BrowseNode(UaNodeId startNodeId, ReferenceDescriptionArray& references, unsigned maxReferences = 0) {
        UA_BrowseDescription* desc = UA_BrowseDescription_new();
        desc->nodeId = startNodeId.GetNodeId();
        desc->browseDirection = UA_BROWSEDIRECTION_FORWARD;
        desc->nodeClassMask = UA_NODECLASS_OBJECT | UA_NODECLASS_METHOD | UA_NODECLASS_VARIABLE;
        desc->includeSubtypes = true;
        desc->referenceTypeId = UA_NODEID_NUMERIC(0, UA_NS0ID_HIERARCHICALREFERENCES);
        desc->resultMask = UA_BROWSERESULTMASK_ALL;

        return Browse(desc, references, maxReferences);
    }

    // TODO if there is more references to return then maxReferences, UA_Server_browse will return 'continuationPoint'
    // which is used to get rest of references with UA_Server_browseNext
    // UA_Server_browseNext(UA_Server *server, UA_Boolean releaseContinuationPoint, const UA_ByteString *continuationPoint);
    // UA_Server_browseRecursive(UA_Server *server, const UA_BrowseDescription *bd, size_t *resultsSize, UA_ExpandedNodeId **results);

    // UA_Server_translateBrowsePathToNodeIds(UA_Server *server, const UA_BrowsePath *browsePath);
    UA_BrowsePathResult TranslateBrowsePathToNodeIds(UA_BrowsePath& bp) {


        UA_BrowsePathResult browsePathResult = UA_Server_translateBrowsePathToNodeIds(_server, &bp);
        return browsePathResult;
    }

    UaNodeId FindSingleNodeId(UaNodeId& originNodeId, const std::string& name, int& namespaceIndex) {
        //UA_STACKARRAY(UA_RelativePathElement, rpe, 2);
        //memset(rpe, 0, sizeof(UA_RelativePathElement) * 2);

        UA_RelativePathElement rpe;
        UA_RelativePathElement_init(&rpe);
        rpe.referenceTypeId = UaNodeId::HasComponent.GetNodeId();
        rpe.isInverse = false;
        rpe.includeSubtypes = false;
        rpe.targetName = UA_QUALIFIEDNAME(namespaceIndex, (char*)&name[0]);

        UA_BrowsePath bp;
        UA_BrowsePath_init(&bp);
        bp.startingNode = originNodeId.GetNodeId();
        bp.relativePath.elementsSize = 1;
        bp.relativePath.elements = &rpe;

        UA_BrowsePathResult result = TranslateBrowsePathToNodeIds(bp);
        if(result.statusCode == UA_STATUSCODE_GOOD && result.targetsSize == 1) {
            return UaNodeId(result.targets[0].targetId.nodeId);
        }
        return UaNodeId::Null;
    }

    // UA_Server_browseSimplifiedBrowsePath(UA_Server *server, const UA_NodeId origin, size_t browsePathSize, const UA_QualifiedName *browsePath);
    UA_BrowsePathResult BrowseSimplifiedBrowsePath(UaNodeId& originNodeId, QualifiedNameArray& names) {
        return UA_Server_browseSimplifiedBrowsePath(_server, originNodeId.GetNodeId(), names.length(), names.data());
    }

    //Search for node id of node with name (node must be direct descendant)
    UaNodeId BrowseSimplifiedSingleNodeId(UaNodeId& originNodeId, const std::string& name, int& namespaceIndex) {
        /*UA_QualifiedName qName;
        qName.namespaceIndex = namespaceIndex;
        qName.name = UA_String_fromChars(&name[0]);*/
        std::string nm = name;
        UA_QualifiedName qName = UA_QUALIFIEDNAME(namespaceIndex, &nm[0]);

        UA_BrowsePathResult result = UA_Server_browseSimplifiedBrowsePath(_server, originNodeId.GetNodeId(), 1, &qName);
        if(result.statusCode == UA_STATUSCODE_GOOD && result.targetsSize == 1) {
            return UaNodeId(result.targets[0].targetId.nodeId);
        }
        return UaNodeId::Null;
    }



    // UA_NodeIteratorCallback
    // UA_Server_forEachChildNodeCall()


    /**
     * Discovery
     * --------- */

     /**
      * Call this method after the register client is connected to correct endpoint on server
      * @return status
      */
    bool RegisterServer() {
        // TODO check if _registerClient is connected
        return UA_Server_register_discovery(_server, _registerClient->GetClient(), nullptr) == UA_STATUSCODE_GOOD;
    }

    bool UnregisterServer() {
        return UA_Server_unregister_discovery(_server, _registerClient->GetClient()) == UA_STATUSCODE_GOOD;
    }

    /**
     *
     * @param endpointUrl
     * @param period
     * @param delay
     * @param periodicCallbackId Id of created periodic callback. Use it for UA_Server_removeCallback when unregister server.
     * @return
     */
    bool AddPeriodicServerRegister(std::string endpointUrl, unsigned period, unsigned delay, unsigned long *periodicCallbackId);

    virtual void ServerRegisterActivity() {}

    /**
     * Override this method for implementing own functionality when some LDS server announces itself on network
     * @param serverOnNetwork
     */
    virtual void ServerOnNetwork(const UA_ServerOnNetwork *serverOnNetwork) {}

    /**
     * Information Model Callbacks
     * ---------------------------
     * Node Lifecycle: Constructors, Destructors and Node Contexts
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
    // UA_GlobalNodeLifecycle constructor/destructor
    // UA_NodeTypeLifecycle constructor/destructor

    // UA_Server_setNodeTypeLifecycle
    bool SetTypeLifeCycle(UaNodeId &typeNodeId, UA_NodeTypeLifecycle nodeTypeLifecycle) {
        return UA_Server_setNodeTypeLifecycle(_server, typeNodeId.GetNodeId(), nodeTypeLifecycle) == UA_STATUSCODE_GOOD;
    }
    // UA_Server_getNodeContext


    // UA_Server_setNodeContext
    bool SetNodeContext(UaNodeId nodeId, void* ctx) {
        return UA_Server_setNodeContext(_server, nodeId.GetNodeId(), ctx) == UA_STATUSCODE_GOOD;
    }


    /**
     * Data Source Callback
     * ^^^^^^^^^^^^^^^^^^^^
     * The server has a unique way of dealing with the content of variables. Instead
     * of storing a variant attached to the variable node, the node can point to a
     * function with a local data provider. Whenever the value attribute is read,
     * the function will be called and asked to provide a UA_DataValue return value
     * that contains the value content and additional timestamps.
     */
     // UA_DataSource read/write
     // UA_Server_setVariableNode_dataSource(UA_Server *server, const UA_NodeId nodeId, const UA_DataSource dataSource);

    /**
     * Value Callback
     * ^^^^^^^^^^^^^^
     * Value Callbacks can be attached to variable and variable type nodes. If
     * not ``NULL``, they are called before reading and after writing respectively.
     */
     // UA_ValueCallback onRead/onWrite
     //UA_Server_setVariableNode_valueCallback(UA_Server *server, const UA_NodeId nodeId, const UA_ValueCallback callback);

    /**
     * Local MonitoredItems
     * ^^^^^^^^^^^^^^^^^^^^ */
     // UA_Server_DataChangeNotificationCallback
     // UA_Server_EventNotificationCallback
     // UA_Server_createDataChangeMonitoredItem
     // UA_Server_deleteMonitoredItem

    /**
     * Method Callbacks
     * ^^^^^^^^^^^^^^^^ */
     // UA_MethodCallback

     // UA_Server_setMethodNode_callback
     bool SetMethodCallback(UaNodeId& methodNodeId, UA_MethodCallback callback) {
        return UA_Server_setMethodNode_callback(_server,
                                         methodNodeId.GetNodeId(),
                                         callback) == UA_STATUSCODE_GOOD;
     }

    /**
     * Interacting with Objects
     * ------------------------ */
     // UA_Server_writeObjectProperty
     // UA_Server_writeObjectProperty_scalar
     // UA_Server_readObjectProperty
     // UA_Server_call(UA_Server *server, const UA_CallMethodRequest *request);

    /**
     * Node Addition and Deletion
     * -------------------------- */
     // UA_Server_addVariableNode
     UaNodeId AddVariableNode(std::string name,
                              std::string browseName,
                              std::string description,
                              UA_Byte accessLevel,
                              UA_UInt16 type,
                              UA_Int32 valueRank,
                              UaNodeId parent,
                              UaNodeId reference = UaNodeId::HasComponent,
                              UaNodeId variableOfType = UaNodeId::BaseDataVariableType,
                              int numberOfDimensions = 0,
                              UA_UInt32* sizeOfEachDimension = nullptr, // array of int
                              void* ctx = nullptr,
                              bool isMandatory = true) {
         UA_VariableAttributes varAttr = UA_VariableAttributes_default;

         varAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US",&name[0]);
         varAttr.description = UA_LOCALIZEDTEXT((char*)"en-US",&description[0]);
         varAttr.accessLevel = accessLevel;
         varAttr.dataType = UA_TYPES[type].typeId;
         varAttr.valueRank = valueRank;

         if (numberOfDimensions != 0 && sizeOfEachDimension != nullptr) {
             // TODO check if numberOfDimensions == sizeOfEachDimension.length()
             // Value rank urcuje rozne kombinacie hodnoty scalar/dimenzie
             // Dimenzie urcuju uz presny pocet dimenzi a ich velkost
             /*
                 n >= 1: the value is an array with the specified number of dimensions
                 n =  0: the value is an array with one or more dimensions
                 n = -1: the value is a scalar
                 n = -2: the value can be a scalar or an array with any number of dimensions
                 n = -3: the value can be a scalar or a one dimensional array
              */
             //[n]  n = pocet dimenzii
             //{y1,y2,..,yn} = y1..yn = dlzka kazdej dimenzie

             varAttr.arrayDimensions = sizeOfEachDimension;
             varAttr.arrayDimensionsSize = numberOfDimensions;
         }

         //varAttr.value;
         /* vAttr.value is left empty, the server instantiates with the default value */

         UaNodeId variableNodeId;

         UA_Server_addVariableNode(_server,
                                   UaNodeId::Null.GetNodeId(),
                                   parent.GetNodeId(),
                                   reference.GetNodeId(),
                                   UA_QUALIFIEDNAME(defaultNamespaceIndex, &browseName[0]),
                                   variableOfType.GetNodeId(),
                                   varAttr,
                                   ctx,
                                   variableNodeId.Get());

         if(isMandatory) {
             UA_Server_addReference(_server, variableNodeId.GetNodeId(),
                                    UaNodeId::HasModellingRule.GetNodeId(),
                                    UA_EXPANDEDNODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULE_MANDATORY), true);
         }

         return variableNodeId;
     }
     // UA_Server_addVariableTypeNode
     UaNodeId AddDataVariableTypeNode(std::string& name,
             std::string& description,
             UA_UInt16 type,
             UA_Variant defValue,
             UA_Int32 valueRank,
             int arrayDimSize,
             UaNodeId variableTypeOf,
             void* typeCtx = nullptr) {
         UA_VariableTypeAttributes varTypeAttr = UA_VariableTypeAttributes_default;

         varTypeAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US",&name[0]);
         varTypeAttr.description = UA_LOCALIZEDTEXT((char*)"en-US",&description[0]);
         varTypeAttr.dataType = UA_TYPES[type].typeId;
         varTypeAttr.valueRank = valueRank;

         if (arrayDimSize != 0) {
             // TODO cant create array with variable in dimension size []
             UA_UInt32 arrayDimensions[1] = {2};
             varTypeAttr.arrayDimensions = arrayDimensions;
             varTypeAttr.arrayDimensionsSize = arrayDimSize;
         }

         /*A matching default value is required*/
         varTypeAttr.value = defValue;

         UaNodeId variableTypeNodeId;

         UA_Server_addVariableTypeNode(_server,
                                       UaNodeId::Null.GetNodeId(),
                                       UaNodeId::BaseDataVariableType.GetNodeId(),
                                       UaNodeId::HasSubType.GetNodeId(),
                                       UA_QUALIFIEDNAME(defaultNamespaceIndex, &name[0]),
                                       variableTypeOf.GetNodeId(),// If this variable type is not derived from another variable type NULL
                                       varTypeAttr,
                                       typeCtx,
                                       variableTypeNodeId.Get());
         return variableTypeNodeId;
     }
     // UA_Server_addObjectNode
     UaNodeId AddObjectNode(std::string name,
             std::string browseName,
             std::string description,
             UaNodeId& parent,
             UaNodeId& reference,
             UaNodeId& objectTypeOf = UaNodeId::BaseObjectType,
             void* nodeCtx = nullptr,
             bool isMandatory = true) {
         UA_ObjectAttributes objectAttr = UA_ObjectAttributes_default;
         objectAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US", &name[0]);
         objectAttr.description = UA_LOCALIZEDTEXT((char*)"en-US", &description[0]);

         UaNodeId objectNodeId;
         if (UA_Server_addObjectNode(_server,
                                     UaNodeId::Null.GetNodeId(),
                                     parent.GetNodeId(),
                                     reference.GetNodeId(),
                                     UA_QUALIFIEDNAME(defaultNamespaceIndex, &browseName[0]),
                                     objectTypeOf.GetNodeId(),
                                     objectAttr,
                                     nodeCtx, // This is ctx for this node ?? what is the difference in xtc if this ndoe will be added to Type or Objects folder as instance?
                                     // Can have object type object instance with HasComponent reference?
                                     objectNodeId.Get()) != UA_STATUSCODE_GOOD) {

             UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Error Adding Object Node %s.", &name[0]);
             return UaNodeId::Null;
         }

         if(isMandatory) {
             bool status = UA_Server_addReference(_server, objectNodeId.GetNodeId(),
                                    UaNodeId::HasModellingRule.GetNodeId(),
                                    UA_EXPANDEDNODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULE_MANDATORY), true) == UA_STATUSCODE_GOOD;
         }

         return objectNodeId;
     }
     // UA_Server_addObjectTypeNode
     bool AddObjectTypeNode(std::string name,
             std::string description,
             UaNodeId& requestedNodeId,
             UaNodeId parentNodeId = UaNodeId::BaseObjectType,
             void* typeCtx = nullptr) {
         UA_ObjectTypeAttributes objectTypeAttr = UA_ObjectTypeAttributes_default;
         objectTypeAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US", &name[0]);
         objectTypeAttr.description = UA_LOCALIZEDTEXT((char*)"en-US", &description[0]);

         if(UA_Server_addObjectTypeNode(_server,
                                     requestedNodeId.GetNodeId(),
                                     UaNodeId::BaseObjectType.GetNodeId(),
                                     UaNodeId::HasSubType.GetNodeId(),
                                     UA_QUALIFIEDNAME(defaultNamespaceIndex, &name[0]),
                                     objectTypeAttr,
                                     typeCtx, // This is node type context? so every node of this type will have acces to this ctx
                                     NULL) != UA_STATUSCODE_GOOD) {
             UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Error Adding Object Type Node %s.", &name[0]);
             return false;
         }
         return true;
     }
     // UA_Server_addViewNode
     // UA_Server_addReferenceTypeNode
     // UA_Server_addDataTypeNode
     // UA_Server_addDataSourceVariableNode
     // UA_Server_addMethodNodeEx
     // UA_Server_addMethodNode
     bool AddMethodNode(std::string name,
             std::string browseName,
             std::string description,
             UaNodeId& requestedNodeId,
             ArgumentList input,
             ArgumentList output,
             UaNodeId& parent = UaNodeId::BaseObjectType,
             UA_MethodCallback callBack = NULL,
             UaNodeId& reference = UaNodeId::HasComponent,
             void* ctx = nullptr,
             bool isMandatory = true) {

         UA_MethodAttributes methodAttr = UA_MethodAttributes_default;
         methodAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US", &name[0]);
         methodAttr.description = UA_LOCALIZEDTEXT((char*)"en-US", &description[0]);
         methodAttr.executable = true;
         methodAttr.userExecutable = true;
         //methodAttr.writeMask
         //methodAttr.userWriteMask
         UaNodeId methodNodeId;
         bool status = UA_Server_addMethodNode(_server,
                                                         requestedNodeId.GetNodeId(),
                                                         parent.GetNodeId(),
                                                         reference.GetNodeId(),
                                                         UA_QUALIFIEDNAME(defaultNamespaceIndex, &browseName[0]),
                                                         methodAttr,
                                                         callBack,
                                                         input.size(), input.GetArguments().data(),
                                                         output.size(), output.GetArguments().data(),
                                                         ctx, methodNodeId.Get()) == UA_STATUSCODE_GOOD;
         //generatedNodeId.IsNull() ? nullptr : generatedNodeId.Get()
         bool statusRef;
         if(isMandatory && status) {
             statusRef = UA_Server_addReference(_server, methodNodeId.GetNodeId(),
                                                UaNodeId::HasModellingRule.GetNodeId(),
                                                UA_EXPANDEDNODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULE_MANDATORY), true) == UA_STATUSCODE_GOOD;
         }
         return isMandatory ? status && statusRef : status;
     }
     // ?????????????????????
     // UA_Server_addNode_begin
     // UA_Server_addNode_finish
     // UA_Server_addMethodNode_finish

     // UA_Server_deleteNode
     bool DeleteNode(UaNodeId nodeId, bool deteleReferences) {
         return UA_Server_deleteNode(_server, nodeId.GetNodeId(), deteleReferences) == UA_STATUSCODE_GOOD;
     }

     // UA_Server_addReference
    /* void AddReference(UaNodeId sourceNodeId,
                       UaNodeId referenceNodeId,
                       UaExpandedNodeId targetNodeId,
                       bool isForward = true) {
         UA_Server_addReference(_server,
                 sourceNodeId.GetNodeId(),
                                referenceNodeId.GetNodeId(),
                                targetNodeId.GetNodeId(),
                                isForward)
     }*/
     // UA_Server_deleteReference

    /**
     * Events
     * ------ */
     // UA_Server_createEvent
     // UA_Server_triggerEvent


     // UA_Server_updateCertificate
    /**
     * Utility Functions
     * ----------------- */

    int AddNamespace(std::string ns) {
        defaultNamespaceIndex = UA_Server_addNamespace(_server, &ns[0]);
        defaultNamespaceIndex = 0;
        return defaultNamespaceIndex;
    }

    int GetNamespaceByName(std::string namespaceUri) {
        size_t index;
        if(UA_Server_getNamespaceByName(_server, ToUA_String(namespaceUri), &index) == UA_STATUSCODE_GOOD)
            return index;

        index = -1;
        return index;
    }

     // UA_Server_AccessControl_allowHistoryUpdateUpdateData
     // UA_Server_AccessControl_allowHistoryUpdateDeleteRawModified
};

#endif //UASERVER_H