//
// Created by magino on 18.7.2019.
//

#ifndef UACLIENT_H
#define UACLIENT_H

#include <thread>
#include <open62541/plugin/log_stdout.h>

#include "UaSubscription.h"
#include "core/UaObjects.h"

typedef std::shared_ptr<UaSubscription> UaSubscriptionRef;
typedef std::map<unsigned, UaSubscriptionRef> UaSubscriptionMap;

class UaClient {
private:
    UA_Client* _client;
    UA_ClientConfig* _config;
    UaSubscriptionMap _subscriptions;

    std::thread _thread;
    bool _running;

    static void StateCallback(UA_Client* client, UA_ClientState clientState);
    static void SubscriptionInactivityCallback(UA_Client* client, UA_UInt32 subscriptionId, void* subContext);
    static void ClientAsyncServiceCallback (UA_Client* client, void* userdata, UA_UInt32 requestId, void* response);
public:
    UaClient();
    ~UaClient();

    virtual void StateChange(UA_ClientState clientState);
    UA_Client* GetClient() const { return _client; };

    /**
    * Client Lifecycle
    * ---------------- */
    UA_ClientState GetState() const { return UA_Client_getState(_client); }
    // return reference? or pointer?
    UA_ClientConfig& GetConfig() const { return *UA_Client_getConfig(_client); }
    void* GetContext() const { return UA_Client_getContext(_client); }
    void ResetClient() { UA_Client_reset(_client); }

    /**
     * Connect to a Server
     * ------------------- */
    bool ConnectAnonymous(const std::string& url) const;
    bool ConnectAnonymousAsync(const std::string& url) const;
    bool ConnectNoSession(const std::string& url) const;
    bool ConnectWithCredentials(const std::string& url, const std::string& userName, const std::string& password) const;

    bool Disconnect();
    bool DisconnectAsync();

    /**
     * Discovery Service set
     * ^^^^^^^^^^^^^^^^^^^^^ */
    bool FindServers(const std::string& url, StringArray& serverUris, StringArray& localeIds, ApplicationDescriptionArray& registeredServers);
    bool FindServersOnNetwork(const std::string& url, unsigned startingRecordId, unsigned maxRecordsToReturn,
                              StringArray& serverCapabilityFilter, ServerOnNetworkArray& serversOnNetwork);
    bool GetEndpoints(const std::string& url, EndpointDescriptionArray& endpoints);

    bool FindServersByUri(const std::string& serverUri,  ApplicationDescriptionArray& registeredServers);
    // TODO Get endpoints with filter

    /*
     * Attribute Service Set
     * ^^^^^^^^^^^^^^^^^^^^^ */
    /**
     * Read Attributes
     * ---------------*/
    // Or more specific in client_high_level.h (for work with one node at once)
    bool ReadAttribute(UaNodeId& nodeId, UA_AttributeId attributeId, const UA_DataType* outDataType, void* outData);
    /**
     * Write Attributes
     * ----------------*/
    // Or more specific in client_high_level.h (for work with one node at once)
    bool WriteAttribute(UaNodeId& nodeId, UA_AttributeId attributeId, const UA_DataType* inDataType, const void* inData);

    /*
    * Historical Access Service Set
    * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
    // UA_Client_Service_historyRead
    // UA_Client_Service_historyUpdate
    // Or more specific in client_high_level.h

    /*
     * Method Service Set
     * ^^^^^^^^^^^^^^^^^^ */
    // UA_Client_Service_call (client.h) || UA_Client_call (client_high_level.h)

    /*
     * NodeManagement Service Set
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^ */
    // UA_Client_Service_addNodes
    // UA_Client_Service_addReferences
    // UA_Client_Service_deleteNodes
    // UA_Client_Service_deleteReferences
    // Or more specific in client_high_level.h (for work with one node at once)

    /*
     * View Service Set
     * ^^^^^^^^^^^^^^^^ */
    // UA_Client_Service_browse
    // UA_Client_Service_browseNext
    // UA_Client_Service_translateBrowsePathsToNodeIds
    // UA_Client_Service_registerNodes
    // UA_Client_Service_unregisterNodes

    /*
     * Query Service Set
     * ^^^^^^^^^^^^^^^^^ */
    // UA_Client_Service_queryFirst
    // UA_Client_Service_queryNext

    /**
     * Misc Highlevel Functionality
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
     // UA_Client_NamespaceGetIndex
     // UA_Client_forEachChildNodeCall

    /**
     * Asynchronous Services
     * --------------------- */
    void Run(unsigned short timeout) {
        _thread = std::thread([this] () {
            _running = true;
            while (_running) {
                UA_Client_run_iterate(_client, 500);
                UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Running!");
            }
        });
    }
    // TODO one thread for more clients

    void Stop() {
        _running = false;
    }

    /**
    * Timed Callbacks
    * --------------- */
    // UA_Client_addTimedCallback
    // UA_Client_addRepeatedCallback
    // UA_Client_changeRepeatedCallbackInterval
    // UA_Client_removeCallback

    /*
     * Subscription Service Set
     * ^^^^^^^^^^^^^^^^^^^^^^^^ */
    // Wrapper in UaSubscription.h with MonitoredItems Service Set
    UaSubscriptionRef CreateSubscription();
    bool RemoveSubscription(unsigned id);
    // RemoveSubscriptions();
};
#endif //UACLIENT_H