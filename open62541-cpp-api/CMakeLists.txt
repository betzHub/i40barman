# CMake version from 3.10 to 3.12, because open62541 library support 3.12 max
cmake_minimum_required(VERSION 3.10...3.12 FATAL_ERROR)
project(open62541-cpp-api)
set(CMAKE_CXX_STANDARD 11)

# Set up Open62541
set(UA_ENABLE_HISTORIZING ON)
set(UA_ENABLE_EXPERIMENTAL_HISTORIZING ON)
set(UA_ENABLE_METHODCALLS ON)
set(UA_ENABLE_NODEMANAGEMENT ON)
set(UA_ENABLE_SUBSCRIPTIONS ON)
set(UA_ENABLE_SUBSCRIPTIONS_EVENTS ON)
set(UA_ENABLE_DISCOVERY ON)
set(UA_ENABLE_DISCOVERY_MULTICAST ON)
set(UA_ENABLE_QUERY ON)
#set(UA_ENABLE_ENCRYPTION ON)
#set(UA_ENABLE_WEBSOCKET_SERVER ON)
set(UA_LOGLEVEL 100)
set(MDNSD_LOGLEVEL 100)
set(UA_NAMESPACE_ZERO FULL)
set(UA_ENABLE_STATUSCODE_DESCRIPTIONS ON)
set(UA_ENABLE_TYPEDESCRIPTION ON)

add_subdirectory(lib/open62541)

# Find all source files with file extension .c/.cpp inside src folder
file(GLOB_RECURSE PROJECT_SRCS "src/*.c" "src/*.cpp" "include/*.h")

# Add source files to project
add_library(${PROJECT_NAME} ${PROJECT_SRCS})

# Include project header files
target_include_directories(${PROJECT_NAME} PUBLIC include)

target_link_libraries(${PROJECT_NAME} PUBLIC open62541::open62541)