//
// Created by magino on 8.6.19.
//

#ifndef MAIN_H
#define MAIN_H

#include <open62541/types.h>
#include "Composite/S7NodeTree.h"

#include "MyUaServer.h"
#include "Communication/ICommunication.h"

static UA_Boolean running = true;
static ICommunication* communication;
static S7NodeTree* nodeTree;

static void stopHandler(int sign);

static void beforeReadTime(UA_Server *server,
                           const UA_NodeId *sessionId, void *sessionContext,
                           const UA_NodeId *nodeId, void *nodeContext,
                           const UA_NumericRange *range, const UA_DataValue *data);

static void afterWriteTime(UA_Server *server,
                           const UA_NodeId *sessionId, void *sessionContext,
                           const UA_NodeId *nodeId, void *nodeContext,
                           const UA_NumericRange *range, const UA_DataValue *data);


#endif //I40BARMAN_MAIN_H
