//
// Created by magino on 2.6.19.
//

#ifndef CSVROWMODEL_H
#define CSVROWMODEL_H

#include <string>
#include <iostream>
#include "SiemensDataType.h"

class S7CsvRowModel {
private:
    UA_NodeClass ParseNodeType(std::string comment) {
        const char type = *comment.substr(0,1).c_str();
        switch(type) {
            case 'o': return UA_NODECLASS_OBJECT;
            case 'm': return UA_NODECLASS_METHOD;
            case 'p': return UA_NODECLASS_VARIABLE;
            default: UA_NODECLASS_UNSPECIFIED;
        }
    }
public:
    std::string name;
    //char* name;
    SiemensDataType data_type;
    UA_DataType ua_data_type;
    double offset;
    bool accessible;
    bool writable;
    bool visible;
    UA_NodeClass nodeType;
    std::string comment;
    int level;
    S7CsvRowModel() {}
    S7CsvRowModel(const std::string& name,
                //char* name,
                const SiemensDataType data_type,
                  const UA_DataType ua_data_type,
                  const double& offset,
                  const std::string& accessible,
                  const std::string& writable,
                  const std::string& visible,
                  const std::string& comment)
    {
        this->name = name;
        this->data_type = data_type;
        this->offset = offset;
        // TODO true musi byt velkym laebo premenne dat do lower case
        this->accessible = accessible == "True";
        this->writable = writable == "True";
        this->visible = visible == "True";
        this->comment = comment;
        nodeType = ParseNodeType(comment);
        this->ua_data_type = ua_data_type;
        level = std::stoi(comment.substr(1, 2));
    }

    virtual ~S7CsvRowModel() {}
    bool IsObject() const { return comment.find('o') != std::string::npos; }
};

#endif //CSVROWMODEL_H
