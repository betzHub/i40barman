//
// Created by magino on 6.6.19.
//

#ifndef CSVCOMPOSITEBUILDER_H
#define CSVCOMPOSITEBUILDER_H

#include <queue>
#include "csv.h"
#include "ICompositeBuilder.h"
#include "S7CsvRowModel.h"
#include "Composite/IS7Node.h"
#include "Composite/S7ObjectNode.h"
#include "Composite/S7VariableNode.h"
#include "Composite/S7MethodNode.h"
#include "SiemensDataType.h"

class S7CsvCompositeBuilder: public ICompositeBuilder {
private:
    ICommunication* comm;
    MyUaServer* uaServer;
    std::queue<S7CsvRowModel*> rows;
    // TODO how to create CSVReader reference without specify parameters
    io::CSVReader<7>* in;
    int actualLevel;

    S7CsvRowModel* GetRow() { return rows.front(); }

    void BuildLevel(IS7Node* objectNode) {
        if(rows.empty())
            return;

        S7CsvRowModel* row = GetRow();

        if(row->level <= actualLevel)
            return;

        if(row->IsObject()) {
            ++actualLevel;
            IS7Node *newObjectNode = new S7ObjectNode(row, comm, uaServer);
            objectNode->Add(newObjectNode);
            rows.pop();

            do {
                BuildLevel(newObjectNode);
            } while (!rows.empty() && rows.front()->level == actualLevel + 1);

            --actualLevel;
        }
        else if (!row->IsObject()) {
            objectNode->Add(new S7VariableNode(row, comm, uaServer));
            rows.pop();
            BuildLevel(objectNode);
        }
    }

    void BuildLevel2(IS7Node* objectNode) {
        if(rows.empty())
            return;

        S7CsvRowModel* row = GetRow();

        if(row->level <= actualLevel)
            return;
        switch (row->nodeType) {
            case UA_NODECLASS_OBJECT: {
                ++actualLevel;
                IS7Node *newObjectNode = new S7ObjectNode(row, comm, uaServer);
                objectNode->Add(newObjectNode);
                rows.pop();

                do {
                    BuildLevel2(newObjectNode);
                } while (!rows.empty() && rows.front()->level == actualLevel + 1);

                --actualLevel;
                break;
            }
            case UA_NODECLASS_METHOD: {
                ++actualLevel;
                S7MethodNode* method = new S7MethodNode(row, comm, uaServer);
                rows.pop();

                method->AddTrigger(rows.front());
                rows.pop();

                method->AddOutputArgument(rows.front());
                rows.pop();

                S7CsvRowModel* methodParam;
                while( !rows.empty() && (methodParam = GetRow())->level == actualLevel + 1) {
                    method->AddInputArgument(methodParam);
                    rows.pop();
                }

                objectNode->Add(method);
                --actualLevel;
                break;
            }
            case UA_NODECLASS_VARIABLE: {
                objectNode->Add(new S7VariableNode(row, comm, uaServer));
                rows.pop();
                BuildLevel2(objectNode);
                break;
            }
        }
    }

public:
    S7CsvCompositeBuilder(std::string fileName, ICommunication* comm, MyUaServer* server) {
        in = new io::CSVReader<7>(fileName);
        this->comm = comm;
        this->uaServer = server;

        // TODO maybe add GetData to interface
        //in.read_header(io::ignore_extra_column, "Name", "Data type", "Offset", "*Start value", "*Retain", "Accessible", "Writable", "Visible", "*Setpoint", "Comment");
        in->read_header(io::ignore_extra_column, "Name", "Data type", "Offset", "Accessible", "Writable", "Visible", "Comment");

        //char* name;
        std::string name, data_type, comment, accessible, writable, visible; double offset;
        //EnumParser<UA_DataType> uaParser; uaParser.ParseSomeEnum(data_type)
        //EnumParser<SiemensDataType> siemensParser; siemensParser.ParseSomeEnum(data_type)
        // TODO String and Array parse [] brackets and number
        while(in->read_row(name, data_type, offset, accessible, writable, visible, comment))
            rows.push(new S7CsvRowModel(name, getS7(data_type), getUa(data_type), offset, accessible, writable, visible, comment));
    }

    UA_DataType getUa(std::string type) {
        if(type == "Bool")
            return UA_TYPES[UA_TYPES_BOOLEAN];
        if(type == "Byte" || type == "USInt")
            return UA_TYPES[UA_TYPES_BYTE];
        if(type == "SInt")
            return UA_TYPES[UA_TYPES_SBYTE];
        if(type == "Int")
            return UA_TYPES[UA_TYPES_INT16];
        if(type == "UInt" || type == "Word")
            return UA_TYPES[UA_TYPES_UINT16];
        if(type == "DInt")
            return UA_TYPES[UA_TYPES_INT32];
        if(type == "UDInt" || type == "DWord")
            return UA_TYPES[UA_TYPES_UINT32];
        if(type == "LInt")
            return UA_TYPES[UA_TYPES_INT64];
        if(type == "ULInt" || type == "LWord")
            return UA_TYPES[UA_TYPES_UINT64];
        if(type == "Real")
            return UA_TYPES[UA_TYPES_FLOAT];
        if(type == "LReal")
            return UA_TYPES[UA_TYPES_DOUBLE];
        if(type == "Char") // TODO this will not work
            return UA_TYPES[UA_TYPES_STRING];
        if(type.compare(0, 6, "String") == 0)
            return UA_TYPES[UA_TYPES_STRING];

        return UA_TYPES[UA_TYPES_EXTENSIONOBJECT];
    }

    SiemensDataType getS7(std::string type) {
        if(type == "Bool")
            return SiemensDataType::BOOL;
        if(type == "Byte" || type == "USInt")
            return SiemensDataType::BYTE;
        if(type == "SInt")
            return SiemensDataType::SINT;
        if(type == "Int")
            return SiemensDataType::INT;
        if(type == "UInt" || type == "Word")
            return SiemensDataType::UINT;
        if(type == "DInt")
            return SiemensDataType::DINT;
        if(type == "UDInt" || type == "DWord")
            return SiemensDataType::UDINT;
        if(type == "LInt")
            return SiemensDataType::LINT;
        if(type == "ULInt" || type == "LWord")
            return SiemensDataType::ULINT;
        if(type == "Real")
            return SiemensDataType::REAL;
        if(type == "LReal")
            return SiemensDataType::LREAL;
        if(type == "Char")
            return SiemensDataType::CHAR;
        if(type.compare(0, 6, "String") == 0)
            return SiemensDataType::STRING;

        return SiemensDataType::UDT;
    }

    virtual ~S7CsvCompositeBuilder() {}

    ICompositeBuilder* BuildRoot() {
        S7CsvRowModel* row = GetRow();
        rootNode = new S7ObjectNode(row, comm, uaServer);
        rows.pop();
        actualLevel = 0;
        return this;
    }

    ICompositeBuilder* BuildTree() {
        do {
            BuildLevel2(rootNode);
        } while(!rows.empty());
        return this;
    }
};

#endif //CSVCOMPOSITEBUILDER_H
