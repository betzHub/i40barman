//
// Created by magino on 27.5.19.
//

#ifndef SIEMENSDATATYPE_H
#define SIEMENSDATATYPE_H

// All data types for S7-300/400, S7-1200 and S7-1500.
// Programming_guideline_DOKU_v12_en.pdf, 03/2014, page 69-70
enum class SiemensDataType {
    // Bit data types
    BOOL = 0,
    BYTE,
    WORD,
    DWORD,
    LWORD,
    // Character type
    CHAR,
    // Numerical data types
    INT,
    DINT,
    REAL,
    SINT,
    USINT,
    UINT,
    UDINT,
    LREAL,
    LINT,
    ULINT,
    // Time types
    TIME,
    DATE,
    TIME_OF_DAY,
    S5TIME,
    LTIME,
    L_TIME_OF_DAY,
    // Data groups that are made up of other data types
    // Time types
    DT, // DATE_AND_TIME
    DTL,
    LDT, // L_DATE_AND_TIME
    // Character type
    STRING, // String[10]
            // -> real data length is 12 bytes
            // -> 1.byte = Total length of string
            // -> 2.byte = actual (real) string length
            // -> this two bytes are followed by 10bytes of data
            // example: String[5] = "Car" => 1.byte = 5, 2.byte = 3, 5 Bytes Data[C,a,r,null,null] or not allocated, not sure
    // Field
    ARRAY,
    // Structure
    STRUCT,
    // Pointer
    POINTER,
    ANY,
    VARIANT,
    // Blocks
    TIMER,
    COUNTER,
    BLOCK_FB,
    BLOCK_FC,
    BLOCK_DB,
    BLOCK_SDB,
    VOID,
    // User defined data type
    UDT,
};

/*
// TODO siemens data type to ua data type
UA_DataType GetUaDataType(SiemensDataType siemens_type) {
    switch (siemens_type) {
        case BOOL: return UA_TYPES[UA_TYPES_BOOLEAN];
        case BYTE: return UA_TYPES[UA_TYPES_BYTE];
        case WORD: return UA_TYPES[UA_TYPES_INT16];
        case DWORD: return UA_TYPES[UA_TYPES_INT32];
        case LWORD: return UA_TYPES[UA_TYPES_INT64];
        case CHAR: return UA_TYPES[UA_TYPES_BYTE];
        case INT: return UA_TYPES[UA_TYPES_INT16];
        case DINT: return UA_TYPES[UA_TYPES_INT32];
        case REAL: return UA_TYPES[UA_TYPES_FLOAT];
        case SINT: return UA_TYPES[UA_TYPES_SBYTE];
        case USINT: return UA_TYPES[UA_TYPES_BYTE];
        case UINT: return UA_TYPES[UA_TYPES_UINT16];
        case UDINT: return UA_TYPES[UA_TYPES_UINT32];
        case LREAL: return UA_TYPES[UA_TYPES_DOUBLE];
        case LINT: return UA_TYPES[UA_TYPES_INT64];
        case ULINT: return UA_TYPES[UA_TYPES_UINT64];
        //case TIME: return UA_TYPES[UA_TYPES_];
        //case DATE: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case TIME_OF_DAY: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case S5TIME: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case LTIME: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case L_TIME_OF_DAY: return UA_TYPES[UA_TYPES_BOOLEAN];
        default: return UA_TYPES[UA_TYPES_EXTENSIONOBJECT];
    }
}
*/
/* TODO string to ua data type
UA_DataType GetUaDataTypeFromString(const std::string siemens_data_type) {
    switch (siemens_data_type) {
        case "BOOL": return UA_TYPES[UA_TYPES_BOOLEAN];
        case "BYTE": return UA_TYPES[UA_TYPES_BYTE];
        case "WORD": return UA_TYPES[UA_TYPES_INT16];
        case "DWORD": return UA_TYPES[UA_TYPES_INT32];
        case "LWORD": return UA_TYPES[UA_TYPES_INT64];
        case "CHAR": return UA_TYPES[UA_TYPES_BYTE];
        case "INT": return UA_TYPES[UA_TYPES_INT16];
        case "DINT": return UA_TYPES[UA_TYPES_INT32];
        case "REAL": return UA_TYPES[UA_TYPES_FLOAT];
        case "SINT": return UA_TYPES[UA_TYPES_SBYTE];
        case "USINT": return UA_TYPES[UA_TYPES_BYTE];
        case "UINT": return UA_TYPES[UA_TYPES_UINT16];
        case "UDINT": return UA_TYPES[UA_TYPES_UINT32];
        case "LREAL": return UA_TYPES[UA_TYPES_DOUBLE];
        case "LINT": return UA_TYPES[UA_TYPES_INT64];
        case "ULINT": return UA_TYPES[UA_TYPES_UINT64];
        //case TIME: return UA_TYPES[UA_TYPES_];
        //case DATE: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case TIME_OF_DAY: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case S5TIME: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case LTIME: return UA_TYPES[UA_TYPES_BOOLEAN];
        //case L_TIME_OF_DAY: return UA_TYPES[UA_TYPES_BOOLEAN];
        default: return UA_TYPES[UA_TYPES_EXTENSIONOBJECT];
    }
}
*/

#endif //SIEMENSDATATYPE_H
