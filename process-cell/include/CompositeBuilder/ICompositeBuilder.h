//
// Created by magino on 6.6.19.
//

#ifndef ICOMPOSITEBUILDER_H
#define ICOMPOSITEBUILDER_H

#include <Composite/IS7Node.h>

class ICompositeBuilder {
protected:
    IS7Node* rootNode;
public:
    virtual ~ICompositeBuilder() {}

    virtual ICompositeBuilder* BuildRoot() = 0;
    virtual ICompositeBuilder* BuildTree() = 0;

    IS7Node* GetRootNode() { return rootNode; }
};

#endif //ICOMPOSITEBUILDER_H
