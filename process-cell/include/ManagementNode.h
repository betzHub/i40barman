//
// Created by magino on 17. 10. 2019.
//

#ifndef MANAGEMENTNODE_H
#define MANAGEMENTNODE_H

#include <core/UaNodeContext.h>
#include "Reservation.h"
#include <iterator>
#include <algorithm>
#include <list>

class ManagementNode: public UaNodeContext {
private:
    UaServer& server;

    UaNodeId reservationsArray;     // Holds all reservations
    Reservation* actualReservation; // just point to the start of vector ??
    std::list<Reservation> reservations;

    int nsIndex;
    // type z vonku by daval zmysel ak naimportujem vlastne typy z XML suboru a cez ctor by som priradoval  node id toho typu a tu by som ho vytvaral
    // => musel by som tu mat niekde este type constructor

    int state = 2;
    int reservationCount;
    int availability = 5;

    int count = 0;

public:
    static UaNodeId typeNodeId;
    // Browse names - are static because i want reach them from everywhere in code
    static const std::string instanceObj;

    static const std::string stateVar;
    static const std::string reservationCountVar;
    static const std::string availabilityVar;
    static const std::string actualReservationUuidVar;

    static const std::string actualReservationObj;
    static const std::string reservationArrayObj;           // Holds all reservation objects

    static const std::string makeReservationMethod;
    static const std::string cancelReservationMethod;
    static const std::string deleteAllReservationsMethod;
    static const std::string confirmReservationMethod;

    static void CreateUaObjectType(UaServer& server, int nsIndex) {
        // i can pass desired node id through method parameters
        // also node id can be created internally
        // for simplicity i will create it here

        UaNodeId typeNodeId(nsIndex, "mngmntType");
        server.AddObjectTypeNode("ManagementType",
                                 "Management take care of reservations for process cell.",
                                    typeNodeId);
        // nemam ako nastavit type context
        // ak by som nastavil context pre variable nodes, tak asi kazdy vytvoreny objekt by ho zdelil?

        server.AddVariableNode("State",
                               ManagementNode::stateVar,
                              "State of management service.",
                              UA_ACCESSLEVELMASK_READ,
                              UA_TYPES_INT32,
                              UA_VALUERANK_SCALAR,
                              typeNodeId);

        server.AddVariableNode("Reservation Count",
                               ManagementNode::reservationCountVar,
                              "Number of successful reservations.",
                              UA_ACCESSLEVELMASK_READ,
                              UA_TYPES_INT32,
                              UA_VALUERANK_SCALAR,
                              typeNodeId);

        server.AddVariableNode("Availability",
                               ManagementNode::availabilityVar,
                              "Availability of the process cell services.",
                              UA_ACCESSLEVELMASK_READ,
                              UA_TYPES_INT32,
                              UA_VALUERANK_SCALAR,
                              typeNodeId);

        server.AddVariableNode("Actual Reservation Uuid",
                             ManagementNode::actualReservationUuidVar,
                             "Uuid of reservation, which is on turn",
                             UA_ACCESSLEVELMASK_READ,
                             UA_TYPES_STRING,
                             UA_VALUERANK_SCALAR,
                             typeNodeId);

        /*server.AddObjectNode("Actual Reservation",
                             ManagementNode::actualReservationObj,
                             "",
                             typeNodeId,
                             UaNodeId::HasComponent,
                             Reservation::typeNodeId,
                             nullptr,
                             false);*/

        server.AddObjectNode("Reservation Array",
                             ManagementNode::reservationArrayObj,
                             "Holds all reservations objects.",
                             typeNodeId,
                             UaNodeId::HasComponent);

        ArgumentList _inMakeRes;
        _inMakeRes.AddScalarArgument("Product Uuid", UA_TYPES[UA_TYPES_STRING].typeIndex, NULL);
        _inMakeRes.AddScalarArgument("CMD", UA_TYPES[UA_TYPES_INT32].typeIndex, NULL);
        _inMakeRes.AddScalarArgument("Operation Order", UA_TYPES[UA_TYPES_INT32].typeIndex, NULL);
        _inMakeRes.AddScalarArgument("Parameter", UA_TYPES[UA_TYPES_INT32].typeIndex, NULL);
        //_inMakeRes.AddArrayArgument("Parameters", UA_TYPES[UA_TYPES_INT32].typeIndex, 5, NULL);
        ArgumentList _outMakeRes;
        _outMakeRes.AddScalarArgument("Status", UA_TYPES[UA_TYPES_BOOLEAN].typeIndex, NULL);
        _outMakeRes.AddScalarArgument("Reservation Uuid", UA_TYPES[UA_TYPES_STRING].typeIndex, NULL);

        bool status = server.AddMethodNode("Make Reservation",
                                            ManagementNode::makeReservationMethod,
                                            "Method for create reservation of desired CMD with parameters.",
                                            UaNodeId::Null,
                                            _inMakeRes,
                                            _outMakeRes,
                                            typeNodeId);


        ArgumentList _inCanRes;
        _inCanRes.AddScalarArgument("Reservation uuid", UA_TYPES[UA_TYPES_STRING].typeIndex, NULL);
        ArgumentList _outCanRes;
        _outCanRes.AddScalarArgument("Status", UA_TYPES[UA_TYPES_BOOLEAN].typeIndex, NULL);

        status = server.AddMethodNode("Cancel Reservation",
                             ManagementNode::cancelReservationMethod,
                             "Method for cancel reservation before process cell server the product.",
                             UaNodeId::Null,
                             _inCanRes,
                             _outCanRes,
                             typeNodeId);

        ArgumentList _inDelAll;
        ArgumentList _outDelAll;
        _outDelAll.AddScalarArgument("Status", UA_TYPES[UA_TYPES_BOOLEAN].typeIndex, NULL);
        status = server.AddMethodNode("Delete all Reservations",
                             ManagementNode::deleteAllReservationsMethod,
                             "Method for delete all reservations.",
                             UaNodeId::Null,
                             _inDelAll,
                             _outDelAll,
                             typeNodeId);

        status = SetTypeLifeCycle(server, typeNodeId);

        ManagementNode::typeNodeId = typeNodeId;
    }

    ManagementNode(UaServer &server, int nsIndex):
            server(server),
            nsIndex(nsIndex) {}

    ~ManagementNode() {}

    void DeleteNode() {
        if(!instanceNodeId.IsNull()) {
            server.DeleteNode(instanceNodeId, true);
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Management Node  with nodeid:%s  has been deleted",
                        instanceNodeId.ToString().c_str());
        }
    }

    // TODO ked som uz dal volitelneho parenta a meno musim aj referenciu!
    bool Instantiate(UaNodeId& parent, UaNodeId& reference, std::string name) {
        instanceNodeId = server.AddObjectNode(name, ManagementNode::instanceObj, "", parent, reference, ManagementNode::typeNodeId, this, false);

        if(!instanceNodeId.IsNull())
            return true;

        return false;
    }

    bool TypeConstruct(UaNodeId& instanceNodeId, UaNodeId& typeNodeId, void* instanceCtx = nullptr) override {
        UaNodeId foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, ManagementNode::stateVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, ManagementNode::reservationCountVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, ManagementNode::availabilityVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, ManagementNode::actualReservationUuidVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }


        reservationsArray = server.BrowseSimplifiedSingleNodeId(instanceNodeId, ManagementNode::reservationArrayObj, nsIndex);

        foundedNodeId = server.FindSingleNodeId(instanceNodeId, ManagementNode::makeReservationMethod, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            server.SetMethodCallback(foundedNodeId, MethodCallback);
        }

        foundedNodeId = server.FindSingleNodeId(instanceNodeId, ManagementNode::cancelReservationMethod, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            server.SetMethodCallback(foundedNodeId, MethodCallback);
        }

        foundedNodeId = server.FindSingleNodeId(instanceNodeId, ManagementNode::deleteAllReservationsMethod, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            server.SetMethodCallback(foundedNodeId, MethodCallback);
        }

        return true;
    }

    void ReadValue(UaNodeId &node, const UA_NumericRange *range, const UA_DataValue *value) override {
        std::string name = server.ReadBrowseName(node);
        UA_Variant val;
        if(name.compare(ManagementNode::stateVar) == 0) {
            UA_Variant_setScalar(&val, &state, &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(ManagementNode::reservationCountVar) == 0) {
            int size = reservations.size();
            UA_Variant_setScalar(&val, &reservationCount, &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(ManagementNode::availabilityVar) == 0) {
            UA_Variant_setScalar(&val, &availability, &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(ManagementNode::actualReservationUuidVar) == 0) {
            if(reservations.size() != 0) {
                UA_String id = ToUA_String(reservations.front().uuid);
                UA_Variant_setScalar(&val, &id, &UA_TYPES[UA_TYPES_STRING]);
                UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
            }
        }
    }
    // Don't need to implement write because all nodes are read only
    void WriteValue(UaNodeId& node, const UA_NumericRange* range, const UA_DataValue& value) override {}

    UA_StatusCode CallBack(UaNodeId methodNodeId,
                           size_t inputSize,
                           const UA_Variant *input,
                           size_t outputSize,
                           UA_Variant *output) {
        std::string name = server.ReadBrowseName(methodNodeId);

        if(name.compare(ManagementNode::makeReservationMethod) == 0) {
            if(inputSize == 4) {
                UA_String productUuid = GetVariantValue<UA_String>(input[0]);
                int cmd = GetVariantValue<int>(input[1]);
                uint operationOrder = GetVariantValue<uint>(input[2]);
                int parameter = GetVariantValue<int>(input[3]);
                //int* parameters = static_cast<int*>(input->data);

                std::string prodId = ToStdString(productUuid);
                std::string resUuid = MakeReservation(prodId, cmd, operationOrder, parameter);

                UA_Boolean returnVal = true;
                UA_Variant_setScalarCopy(output, &returnVal, &UA_TYPES[UA_TYPES_BOOLEAN]);

                // TODO returned string is always NONE why?
                UA_String uuid = ToUA_String(resUuid);
                UA_Variant_setScalarCopy(output+1, &resUuid, &UA_TYPES[UA_TYPES_STRING]);
            }
            else return UA_STATUSCODE_BADMETHODINVALID;
        }

        if(name.compare(ManagementNode::cancelReservationMethod) == 0) {
            if(inputSize == 1) {
                UA_String resUuid = GetVariantValue<UA_String>(input[0]);
                std::string uuid = ToStdString(resUuid);

                UA_Boolean returnVal = CancelReservation(uuid);
                UA_Variant_setScalarCopy(output, &returnVal, &UA_TYPES[UA_TYPES_BOOLEAN]);
            } else return UA_STATUSCODE_BADMETHODINVALID;
        }

        if(name.compare(ManagementNode::deleteAllReservationsMethod) == 0) {
            for(auto& res: reservations)
                res.DeleteNode();

            reservations.clear();

            UA_Boolean returnVal = true;
            UA_Variant_setScalarCopy(output, &returnVal, &UA_TYPES[UA_TYPES_BOOLEAN]);
        }
        return UA_STATUSCODE_GOOD;
    }

    std::string MakeReservation(std::string& productUuid, int& cmd, uint& operationOrder, int& parameter) {
        /*
        int n = sizeof(parameters)/sizeof(parameters[0]);
        std::vector<int> params(parameters, parameters+n);
        */
        std::string uuid_name = "res" + std::to_string(count);
        reservations.push_back(Reservation(server, productUuid, cmd, operationOrder, parameter, uuid_name));

        if (reservations.back().Instantiate(reservationsArray, UaNodeId::HasComponent, uuid_name)) {
            ++count;
            return uuid_name;
        }

        // ak je to prva rezervacia ktoru vkladam je aj aktualna
        // potom ak sa dana rezervacia vykona servis vyroby zmaze rezervaciu? alebo produkt zmaze staru rezervaciu? -> metoda DeleteReservation()
        // v tejto metode sa z queue vyhodi prva rezervacia (pop() a zavola sa destruktor) a zmaze sa node Actual reservation
        // dalej sa nastavi nasledujuca rezervacia z queue ako aktualna

        reservations.pop_back();
        return "";
    }

    bool CancelReservation(std::string& reservationUuid) {
        /*int i = 0;
        for(auto &res: ress) {
            if(res.uuid.compare(reservationUuid) == 0) {
                res.DeleteNode();
                ress.remove(res);
                return true;
            }
            ++i;
        }*/ // Musel by som implementovat == operator
        for (auto itr = reservations.begin(); itr != reservations.end(); ++itr) {
            if((*itr).uuid.compare(reservationUuid) == 0) {
                (*itr).DeleteNode();
                reservations.erase(itr);
                return true;
            }
        }
        return false;
    }

    // Not implemented yet
    void ConfirmReservation() {}

    // This method needs to be called when reservation will be processed by process cell
    void ReservationDone(std::string& reservationUuid) {
        // ak mazem rezervaciu ktora je prva v poradi musim prehodit
    }
};
UaNodeId ManagementNode::typeNodeId = UaNodeId();

const std::string ManagementNode::instanceObj = "Management";

const std::string ManagementNode::stateVar = "State";
const std::string ManagementNode::reservationCountVar = "ReservationCount";
const std::string ManagementNode::availabilityVar = "Availability";
const std::string ManagementNode::actualReservationUuidVar = "ActualReservationUuid";

const std::string ManagementNode::actualReservationObj = "ActualReservation";
const std::string ManagementNode::reservationArrayObj = "ReservationArray";

const std::string ManagementNode::makeReservationMethod = "MakeReservation";
const std::string ManagementNode::cancelReservationMethod = "CancelReservation";
const std::string ManagementNode::deleteAllReservationsMethod = "DeleteAllReservations";
const std::string ManagementNode::confirmReservationMethod = "ConfirmReservation;";

#endif //MANAGEMENTNODE_H