//
// Created by magino on 20.5.2019.
//

#ifndef MODBUSCLIENTCFG_H
#define MODBUSCLIENTCFG_H

#include <string>
#include <nlohmann/json.hpp>

class ModbusTcpClientCfg {
public:
    std::string ipAddress;
    uint16_t port;
    unsigned int cycleTime;
    unsigned int minAddress;
    unsigned int maxAddress;

    ModbusTcpClientCfg(){};
    ~ModbusTcpClientCfg(){};

    ModbusTcpClientCfg(const nlohmann::json& j) {
        ipAddress = j["IpAddress"];
        port = j["Port"];
        cycleTime = j["CycleTime"];
        minAddress = j["MinAddress"];
        maxAddress = j["MaxAddress"];
    }

    inline void to_json(nlohmann::json& j, const ModbusTcpClientCfg& c) {
        j["IpAddress"] = c.ipAddress;
        j["Port"] = c.port;
        j["CycleTime"] = c.cycleTime;
        j["MinAddress"] = c.minAddress;
        j["MaxAddress"] = c.maxAddress;
    }

    inline void from_json(const nlohmann::json& j, ModbusTcpClientCfg& c) {
        j.at("IpAddress").get_to(c.ipAddress);
        j.at("Port").get_to(c.port);
        j.at("CycleTime").get_to(c.cycleTime);
        j.at("MinAddress").get_to(c.minAddress);
        j.at("MaxAddress").get_to(c.maxAddress);
    }
};

#endif //MODBUSCLIENTCFG_H
