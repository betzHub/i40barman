//
// Created by magino on 20.4.19.
//

#ifndef UASERVERCFG_H
#define UASERVERCFG_H

#include <string>
#include <nlohmann/json.hpp>

class UaServerCfg {
public:
    unsigned short int port;
    std::string appName;
    std::string appUri;
    std::string productUri;
    std::string mDnsName;

    UaServerCfg(){};
    ~UaServerCfg(){};

    UaServerCfg(const nlohmann::json& j) {
        port = j["Port"];
        appName = j["AppName"];
        appUri = j["AppUri"];
        productUri = j["ProductUri"];
        mDnsName = j["mDnsName"];
    }

    inline void to_json(nlohmann::json& j, const UaServerCfg& c) {
        j["Port"] = c.port;
        j["AppName"] = c.appName;
        j["AppUri"] = c.appUri;
        j["ProductUri"] = c.productUri;
        j["mDnsName"] = c.mDnsName;
    }

    inline void from_json(const nlohmann::json& j, UaServerCfg& c) {
        j.at("Port").get_to(c.port);
        j.at("AppName").get_to(c.appName);
        j.at("AppUri").get_to(c.appUri);
        j.at("ProductUri").get_to(c.productUri);
        j.at("mDnsName").get_to(c.mDnsName);
    }
};

#endif //UASERVERCFG_H