//
// Created by magino on 30.4.19.
//

#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <fstream>
#include "Configuration/UaServerCfg.h"
#include "Configuration/ModbusTcpClientCfg.h"
#include <nlohmann/json.hpp>

class Configuration {

private:
    nlohmann::json j;

public:
    Configuration();
    ~Configuration(){}
    UaServerCfg GetServerConfig();
    ModbusTcpClientCfg GetModbusClientConfig();
};

#endif //CONFIGURATION_H