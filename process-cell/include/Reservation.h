//
// Created by magino on 5. 10. 2019.
//

#ifndef Reservation_H
#define Reservation_H

#include "core/UaNodeContext.h"

class Reservation: public UaNodeContext {
private:
    UaServer& server;
    int nsIndex;
public:
    std::string uuid;
    std::string productUuid;
    int cmd;
    uint operationOrder;

    std::vector<int> parameters;
    int parameter;

    //long timeOfReservation;
    //long timeLeft;

    static UaNodeId typeNodeId;
    // Browse names - are static because i want reach them from everywhere in code
    static const std::string instanceObj;

    static const std::string reservationUuidVar;
    static const std::string productUuidVar;
    static const std::string cmdVar;
    static const std::string operationOrderVar;
    static const std::string parametersVarArray;
    static const std::string parameterVar;

    static void CreateUaObjectType(UaServer& server, int nsIndex) {
        UaNodeId typeNodeId(nsIndex, "rsrvtnType");
        server.AddObjectTypeNode("ReservationType",
                                 "Object, which holds data of reservations.",
                                 typeNodeId);

        server.AddVariableNode("Reservation Uuid",
                               Reservation::reservationUuidVar,
                               "Universally unique id of reservation.",
                               UA_ACCESSLEVELMASK_READ,
                //UA_TYPES_GUID, // or string
                               UA_TYPES_STRING,
                               UA_VALUERANK_SCALAR,
                               typeNodeId);

        server.AddVariableNode("Product Uuid",
                               Reservation::productUuidVar,
                               "Universally unique id of product which made reservation.",
                               UA_ACCESSLEVELMASK_READ,
                //UA_TYPES_GUID, // or string
                               UA_TYPES_STRING,
                               UA_VALUERANK_SCALAR,
                               typeNodeId);

        server.AddVariableNode("CMD",
                               Reservation::cmdVar,
                               "Command which is reserved in process cell.",
                               UA_ACCESSLEVELMASK_READ,
                               UA_TYPES_INT32,
                               UA_VALUERANK_SCALAR,
                               typeNodeId);

        // TODO nedava zmysel asi lebo produkt is bude rezervovat po jednom, tak bunka nema ako preskladat poradie
        server.AddVariableNode("Operation Order",
                               Reservation::operationOrderVar,
                               "Operation order in recepture.",
                               UA_ACCESSLEVELMASK_READ,
                               UA_TYPES_INT32,
                               UA_VALUERANK_SCALAR,
                               typeNodeId);

        server.AddVariableNode("Parameters",
                               Reservation::parametersVarArray,
                               "Int32[5] Parameters of reserved CMD in process cell.",
                               UA_ACCESSLEVELMASK_READ,
                               UA_TYPES_INT32,
                               UA_VALUERANK_ONE_DIMENSION,
                               typeNodeId,
                               UaNodeId::HasComponent,
                               UaNodeId::BaseDataVariableType,
                               1,
                               new UA_UInt32[1]{5});

        server.AddVariableNode("Parameter",
                               Reservation::parametersVarArray,
                               "Parameter of reserved CMD in process cell.",
                               UA_ACCESSLEVELMASK_READ,
                               UA_TYPES_INT32,
                               UA_VALUERANK_SCALAR,
                               typeNodeId);

        SetTypeLifeCycle(server, typeNodeId);
        Reservation::typeNodeId = typeNodeId;
    }

    Reservation(UaServer& server,
                std::string productUuid,
                int cmd,
                uint operationOrder,
                std::vector<int> parameters,
                std::string resUuid):
                    server(server),
                    uuid(resUuid),
                    productUuid(productUuid),
                    cmd(cmd),
                    operationOrder(operationOrder),
                    parameters(parameters),
                    nsIndex(0) {}

    Reservation(UaServer& server,
                std::string productUuid,
                int cmd,
                uint operationOrder,
                int parameter,
                std::string resUuid):
            server(server),
            uuid(resUuid),
            productUuid(productUuid),
            cmd(cmd),
            operationOrder(operationOrder),
            parameter(parameter),
            nsIndex(0) {
        parameters.push_back(parameter);
    }

    // Copy constructor
    Reservation(const Reservation& reservation):
        server(reservation.server),
        uuid(reservation.uuid),
        productUuid(reservation.productUuid),
        cmd(reservation.cmd),
        operationOrder(reservation.operationOrder),
        parameter(reservation.parameter),
        parameters(reservation.parameters),
        nsIndex(reservation.nsIndex) {
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Reservation Copied");
    }
    // Move constructor
    Reservation(Reservation&& reservation):
        server(reservation.server),
        uuid(reservation.uuid),
        productUuid(reservation.productUuid),
        cmd(reservation.cmd),
        operationOrder(reservation.operationOrder),
        parameter(reservation.parameter),
        parameters(reservation.parameters),
        nsIndex(reservation.nsIndex) {
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Reservation Moved");
    }

    Reservation& operator=(Reservation) throw() { return *this; }

    ~Reservation() {
        //if(!instanceNodeId.IsNull())
        //   DeleteNode();
    }

    void DeleteNode() {
        if(!instanceNodeId.IsNull()) {
            server.DeleteNode(instanceNodeId, true);
            UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Reservation with node id:%s  has been deleted",
                        instanceNodeId.ToString().c_str());
        }
    }

    bool Instantiate(UaNodeId& parent, UaNodeId& reference, std::string name) {
        instanceNodeId = server.AddObjectNode(name, Reservation::instanceObj, "", parent, reference, Reservation::typeNodeId, this, false);

        if(!instanceNodeId.IsNull())
            return true;

        return false;
    }

    bool TypeConstruct(UaNodeId& instanceNodeId, UaNodeId& typeNodeId, void *instanceCtx = nullptr) override {
        UaNodeId foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, Reservation::reservationUuidVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, Reservation::productUuidVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, Reservation::cmdVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, Reservation::operationOrderVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, Reservation::parametersVarArray, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }

        foundedNodeId = server.BrowseSimplifiedSingleNodeId(instanceNodeId, Reservation::parameterVar, nsIndex);
        if(!foundedNodeId.IsNull()) {
            server.SetNodeContext(foundedNodeId, this);
            SetValueCallback(server, foundedNodeId);
        }
        return true;
    }

    void ReadValue(UaNodeId &node, const UA_NumericRange *range, const UA_DataValue *value) override {
        std::string name = server.ReadBrowseName(node);
        UA_Variant val;
        if(name.compare(Reservation::reservationUuidVar) == 0) {
            UA_String resUuid = ToUA_String(uuid);
            UA_Variant_setScalar(&val, &resUuid, &UA_TYPES[UA_TYPES_STRING]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(Reservation::productUuidVar) == 0) {
            UA_String prodUuid = ToUA_String(productUuid);
            UA_Variant_setScalar(&val, &prodUuid, &UA_TYPES[UA_TYPES_STRING]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(Reservation::cmdVar) == 0) {
            UA_Variant_setScalar(&val, &cmd, &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(Reservation::operationOrderVar) == 0) {
            UA_Variant_setScalar(&val, &operationOrder, &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(Reservation::parametersVarArray) == 0) {
            UA_Variant_setArray(&val, &parameters[0], parameters.size(), &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }

        if(name.compare(Reservation::parameterVar) == 0) {
            UA_Variant_setScalar(&val, &parameter, &UA_TYPES[UA_TYPES_INT32]);
            UA_Server_writeValue(server.GetServer(), node.GetNodeId(), val);
        }
    }

    void WriteValue(UaNodeId &node, const UA_NumericRange *range, const UA_DataValue &value) override {}
};
UaNodeId Reservation::typeNodeId = UaNodeId();

const std::string Reservation::instanceObj = "Reservation";

const std::string Reservation::reservationUuidVar = "ReservationUuid";
const std::string Reservation::productUuidVar = "ProductUuid";
const std::string Reservation::cmdVar = "CMD";
const std::string Reservation::operationOrderVar = "OperationOrder";
const std::string Reservation::parametersVarArray = "Parameters";
const std::string Reservation::parameterVar = "Parameter";
#endif //Reservation_H