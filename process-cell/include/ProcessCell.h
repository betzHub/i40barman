//
// Created by magino on 6. 10. 2019.
//

#ifndef PROCESSCELL_H
#define PROCESSCELL_H

#include <iostream>
#include <vector>
#include <map>
#include <server/UaServer.h>
#include "Reservation.h"

class ProcessCell {
private:
    UaServer& server;
public:
    std::string name;
    std::string uuid;
    std::string serialNumber;
    std::string manufacturer;

    std::string ipAddress;
    std::string plcIpAddress;

    void RunAction() {}
};

#endif //PROCESSCELL_H
