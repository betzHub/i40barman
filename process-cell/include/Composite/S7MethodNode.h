//
// Created by magino on 16. 9. 2019.
//

#ifndef S7METHODNODE_H
#define S7METHODNODE_H

#include "IS7Node.h"

class S7MethodNode: public IS7Node {
private:
    ArgumentList _in;
    ArgumentList _out;

    S7CsvRowModel trigger;

    static UA_StatusCode MethodCallback(UA_Server *server, const UA_NodeId *sessionId,
                                       void *sessionContext, const UA_NodeId *methodId,
                                       void *methodContext, const UA_NodeId *objectId,
                                       void *objectContext, size_t inputSize,
                                       const UA_Variant *input, size_t outputSize,
                                       UA_Variant *output)
    {
        if(methodContext) {
            S7MethodNode *method = (S7MethodNode*)methodContext;
            method->CallBack(inputSize, input, outputSize, output);
        }
    }

public:
    S7MethodNode() {}
    S7MethodNode(S7CsvRowModel* row, ICommunication* comm, MyUaServer* server) {
        this->row = row;
        this->comm = comm;
        this->uaServer = server;
    }

    virtual ~S7MethodNode() {}
    virtual void AddOpcUaNode(UA_NodeId parent) {
        char* name = stringToPChar(&row->name);

        UA_MethodAttributes methodAttr = UA_MethodAttributes_default;
        methodAttr.description = UA_LOCALIZEDTEXT((char*)"en-US",name);
        methodAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US",name);
        methodAttr.executable = true;
        methodAttr.userExecutable = true;

        nodeId = UA_NODEID_STRING(parent.namespaceIndex, name);

        UA_Server_addMethodNode(uaServer->GetServer(), nodeId,
                                parent,
                                UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                                UA_QUALIFIEDNAME(parent.namespaceIndex, name),
                                methodAttr, &MethodCallback,
                                _in.size(), _in.GetArguments().data(),
                                _out.size(), _out.GetArguments().data(),
                                (void*)this, NULL);
    }

    virtual IS7Node* FindNode(const UA_NodeId* uaNodeId) {
        if(UA_NodeId_equal(&nodeId, uaNodeId))
            return this;
        else return nullptr;
    }

    virtual void* GetCommParam() { return row; }
    virtual UA_Variant ReadValue() {}

    void AddTrigger(S7CsvRowModel* row) {
        trigger = *row;
    }

    void AddOutputArgument(S7CsvRowModel* row) {
        _out.AddScalarArgument(row->name.c_str(), row->ua_data_type.typeIndex, row);
    }

    void AddInputArgument(S7CsvRowModel* row) {
        _in.AddScalarArgument(row->name.c_str(), row->ua_data_type.typeIndex, row);
    }

    void CallBack(size_t inputSize, const UA_Variant *input,
                  size_t outputSize, UA_Variant *output) {

        for(int i = 0; i < inputSize; ++i) {
            comm->WriteValue(input[i], _in.at(i).context);
        }
        UA_Variant runMethod;
        UA_Boolean run = true;
        UA_Variant_setScalarCopy(&runMethod, &run, &UA_TYPES[UA_TYPES_BOOLEAN]);

        // Write boolean run method
        comm->WriteValue(runMethod, &trigger);

        sleep(1);

        UA_Variant returnVal = comm->GetValue(_out.at(0).context);
        UA_Variant_setScalarCopy(output, returnVal.data, &UA_TYPES[UA_TYPES_BOOLEAN]);
    }
};

#endif //S7METHODNODE_H
