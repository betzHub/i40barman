//
// Created by magino on 27.5.19.
//

#ifndef S7OBJECTNODE_H
#define S7OBJECTNODE_H

#include <string>
#include <vector>
#include "IS7Node.h"
#include "core/UaObjects.h"

class S7ObjectNode: public IS7Node {
private:
    std::vector<IS7Node*> nodes;
public:
    S7ObjectNode(S7CsvRowModel* row, ICommunication* comm, MyUaServer* server) {
        this->row = row;
        this->comm = comm;
        this->uaServer = server;
    }

    ~S7ObjectNode() {}
    virtual void Add(IS7Node *node) { nodes.push_back(node); }

    virtual void AddOpcUaNode(UA_NodeId parent) {
        char* name = stringToPChar(&row->name);

        UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
        oAttr.displayName = UA_LOCALIZEDTEXT((char*)"en-US", name);
        //variableNode.description = UA_LOCALIZEDTEXT((char*)"en-US", row->comment);

        nodeId = UA_NODEID_STRING(1, name);
        if (UA_Server_addObjectNode(uaServer->GetServer(),
                                nodeId,
                                parent,
                                UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                UA_QUALIFIEDNAME(1, name),
                                UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                oAttr,
                                NULL,
                                NULL) != UA_STATUSCODE_GOOD) {
            UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Error Adding Object Node %s.", name);
            return;
        }

        // Build all children
        for (auto &node : nodes) {
            node->AddOpcUaNode(nodeId);
        }
    }

    virtual IS7Node* FindNode(const UA_NodeId* uaNodeId) {
        if(UA_NodeId_equal(&nodeId, uaNodeId))
            return this;
        else {
            for (auto &node : nodes) {
                IS7Node* tmpNode = node->FindNode(uaNodeId);
                if(tmpNode) return tmpNode;
            }
        }

        return nullptr;
    }

    virtual void* GetCommParam() { return row; }
};

#endif //OBJECTNODE_H
