//
// Created by magino on 6.6.19.
//

#ifndef NODETREE_H
#define NODETREE_H

#include <memory>
#include <Communication/ICommunication.h>
#include "IS7Node.h"
#include <open62541/nodeids.h>

class S7NodeTree {
private:
    IS7Node* root;
    const UA_NodeId rootNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);

public:
    S7NodeTree(IS7Node* root) {
        this->root = root;
    }

    void BuildOpcUaAddressSpace() const {
        root->AddOpcUaNode(rootNodeId);
    }

    IS7Node* FindNode(const UA_NodeId* nodeId) const {
        return root->FindNode(nodeId);
    }
};

#endif //NODETREE_H
