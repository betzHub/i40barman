//
// Created by magino on 26.5.19.
//

#ifndef INODE_H
#define INODE_H

#include <string>
#include <open62541/types.h>
#include <CompositeBuilder/S7CsvRowModel.h>
#include <MyUaServer.h>

class IS7Node {
private:
protected:
    ICommunication* comm;
    MyUaServer* uaServer;
    UA_NodeId nodeId;
    // TODO Node should be independent from communication -> refactor!
    S7CsvRowModel* row;

    /*
    std::string name;
    SiemensDataType data_type;
    UA_DataType ua_data_type;
    double offset;
    bool accessible;
    bool writable;
    bool visible;
    std::string comment;
    */
public:
    virtual ~IS7Node() {}

    virtual void Add(IS7Node *node) {};
    virtual void AddOpcUaNode(UA_NodeId parent) = 0;
    virtual IS7Node* FindNode(const UA_NodeId* uaNodeId) = 0;
    virtual void* GetCommParam() = 0;

    UA_NodeId GetNodeId() { return nodeId; };
    UA_DataType GetDataType() { return row->ua_data_type; };

    // These two implement just in PropertyNode
    virtual UA_Variant ReadValue() {};
    //virtual void WriteValue(UA_Variant variant) {};
};

#endif //INODE_H
