//
// Created by magino on 27.5.19.
//

#ifndef VARIABLENODE_H
#define VARIABLENODE_H

#include "IS7Node.h"
#include "main.h"
#include "core/UaObjects.h"

class S7VariableNode : public IS7Node {
public:
    S7VariableNode(S7CsvRowModel* row, ICommunication* comm, MyUaServer* server) {
        this->row = row;
        this->comm = comm;
        this->uaServer = server;
    }

    virtual ~S7VariableNode() {}

    virtual void AddOpcUaNode(UA_NodeId parent) {
        char* name = stringToPChar(&row->name);

        UA_VariableAttributes variableNode = UA_VariableAttributes_default;
        variableNode.displayName = UA_LOCALIZEDTEXT((char*)"en-US", name);
        //variableNode.description = UA_LOCALIZEDTEXT((char*)"en-US", row->comment);
        variableNode.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
        variableNode.dataType = row->ua_data_type.typeId;

        UA_Variant val = comm->GetValue(row);
        UA_Variant_copy(&val, &variableNode.value);

        nodeId = UA_NODEID_STRING(1, name);
        if (UA_Server_addVariableNode(uaServer->GetServer(),
                                      nodeId,
                                      parent,
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                      UA_QUALIFIEDNAME(1, name),
                                      UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                      variableNode,
                                      NULL,
                                      NULL) != UA_STATUSCODE_GOOD) {
            UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Error Adding Variable Node %s.", name);
            return;
        }

        UA_ValueCallback callback;
        callback.onRead = beforeReadTime;
        callback.onWrite = afterWriteTime;
        UA_Server_setVariableNode_valueCallback(uaServer->GetServer(), nodeId, callback);

        // TODO refactor with UA_Server_setNodeContext(UA_Server *server, UA_NodeId nodeId, void *nodeContext);
        // Node context is service for read/write data
    }

    virtual IS7Node* FindNode(const UA_NodeId* uaNodeId) {
        if(UA_NodeId_equal(&nodeId, uaNodeId))
            return this;
        else return nullptr;
    }

    virtual void* GetCommParam() { return row; }
    virtual UA_Variant ReadValue() {}
};

#endif //VARIABLENODE_H