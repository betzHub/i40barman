//
// Created by magino on 20.5.2019.
//

#ifndef MODBUSCLIENT_H
#define MODBUSCLIENT_H

#include <chrono>
#include <memory>
#include <bitset>

#include <open62541/types.h>
#include <open62541/types_generated.h>
#include <modbus/modbus.h>

#include "CompositeBuilder/S7CsvRowModel.h"
#include "Configuration/ModbusTcpClientCfg.h"
#include "ICommunication.h"
#include <mutex>

class ModbusTcpClient: public ICommunication {
private:
    // TODO extract to own file
    enum class Endian {
        big_endian,
        little_endian
    };

    modbus_t* modbus;
    std::mutex lock;

    bool isConnected;
    ModbusTcpClientCfg& config;
    uint16_t* buffer;

    Endian endian;
    int startAddress;
    int endAddress;

    UA_Boolean GetBoolAt(float offset);
    /*UA_Byte GetByte(int pos);
    UA_SByte GetSByte(int pos);
    UA_Int16 GetInt16(int pos);
    UA_UInt16 GetUInt16(int pos);
    UA_Int32 GetInt32(int pos);
    UA_UInt32 GetUInt32(int pos);
    UA_Int64 GetInt64(int pos);
    UA_UInt64 GetUInt64(int pos);
    UA_Float GetFloat(int pos);
    UA_Double GetDouble(int pos);
    UA_String GetChar(int pos);*/
    UA_String GetString(int pos);


    void WriteBoolAt(UA_Boolean val, float offset);
    void WriteByteAt(UA_Byte val, int offset);
    void WriteSByteAt(UA_SByte val, int offset);
    void WriteInt16At(UA_Int16 val, int pos);
    void WriteUInt16At(UA_UInt16 val, int pos);
    void WriteInt32At(UA_Int32 val, int pos);
    void WriteUInt32At(UA_UInt32 val, int pos);
    void WriteInt64At(UA_Int64 val, int pos);
    void WriteUInt64At(UA_UInt64 val, int pos);
    void WriteFloatAt(UA_Float val, int pos);
    void WriteDoubleAt(UA_Double val, int pos);
    /* void WriteCharAt(UA_String val, int pos);*/
    void WriteStringAt(UA_String val, int pos);

    int GetAddressDiff() { return endAddress-startAddress; }
    /***
     * Calculate modbus register number from byte position in data block
     * @param pos of variable in data block
     * @return position in modbus register
     */
    int GetModbusRegisterPos(int pos);

    virtual void Reconnect(volatile bool* running);
    virtual int GetCycleTime();

public:
    ModbusTcpClient(ModbusTcpClientCfg cfg);
    ~ModbusTcpClient() {
        StopComm();
        delete[] buffer;
    }

    virtual void GetData();
    virtual void StopComm();

    virtual UA_Variant GetValue(void* params);
    virtual void WriteValue(UA_Variant data, void* params);
};

#endif //I40BARMAN_MODBUSCLIENT_H
