//
// Created by magino on 11.4.19.
//

#include "MyUaServer.h"

MyUaServer::MyUaServer(UaServerCfg cfg): UaServer(cfg.port, nullptr, BuildAppDescription(cfg), BuildmDnsConfiguration(cfg)) {}

void MyUaServer::ServerOnNetwork(const UA_ServerOnNetwork *serverOnNetwork) {
    // TODO podla application name sa rozhodnut ci sa zaregistrovat alebo nie
    char* discoveryUrl = UaStringToPChar(&serverOnNetwork->discoveryUrl);
    // I can not register on myself
    if(strcmp(discoveryUrl, MY_SERVER_URL) == 0) {
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,"[%d] Not registering on myself", foundedLDS);
        UA_free(discoveryUrl);
        return;
    }

    UaClient* registerClient = GetRegisterClient();
    if(!registerClient) {
        UA_LOG_FATAL(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                     "Could not create the client for remote registering");
        UA_free(discoveryUrl);
        return;
    }

    UA_EndpointDescription* endpointRegister = GetRegisterEndpointFromServer(registerClient->GetClient(), discoveryUrl);
    UA_free(discoveryUrl);

    if(!endpointRegister || endpointRegister->securityMode == UA_MESSAGESECURITYMODE_INVALID) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
                     "Could not find any suitable endpoints on discovery server");
        return;
    }

    char *endpointUrl =  UaStringToPChar(&endpointRegister->endpointUrl);

    UA_StatusCode retVal = UA_Server_addPeriodicServerRegisterCallback(GetServer(), registerClient->GetClient(), endpointUrl, 10 * 60 * 1000, 500, NULL);

    if(retVal != UA_STATUSCODE_GOOD) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER,
                     "Could not create periodic job for server register. StatusCode %s",
                     UA_StatusCode_name(retVal));
        UA_free(endpointUrl);
        return;
    }
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Registered to %s", endpointUrl);
}


UA_EndpointDescription* MyUaServer::GetRegisterEndpointFromServer(UA_Client* client, const char *discoveryServerUrl) {

    UA_EndpointDescription *endpointArray = NULL;
    size_t endpointArraySize = 0;

    UA_StatusCode retVal = UA_Client_getEndpoints(client,
                                                  discoveryServerUrl,
                                                  &endpointArraySize,
                                                  &endpointArray);
    if(retVal != UA_STATUSCODE_GOOD) {
        UA_Array_delete(endpointArray, endpointArraySize, &UA_TYPES[UA_TYPES_ENDPOINTDESCRIPTION]);
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "GetEndpoints failed with %s", UA_StatusCode_name(retVal));
        return NULL;
    }

    UA_EndpointDescription *returnEndpoint = NULL;

    for(size_t i = 0; i < endpointArraySize; ++i) {

        // TODO later change to endpoint with security
        if(!UA_String_equal(&endpointArray[i].securityPolicyUri, &UA_SECURITY_POLICY_NONE_URI))
            continue;

        returnEndpoint = UA_EndpointDescription_new();
        UA_EndpointDescription_copy(&endpointArray[i], returnEndpoint);
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Using LDS endpoint with security None");
    }

    UA_Array_delete(endpointArray, endpointArraySize, &UA_TYPES[UA_TYPES_ENDPOINTDESCRIPTION]);

    return returnEndpoint;
}