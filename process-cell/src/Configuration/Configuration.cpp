//
// Created by magino on 30.4.19.
//

#include <Configuration/Configuration.h>

Configuration::Configuration() {
    // TODO replace name file with constant, and also all strings in cfg
    std::ifstream ifs{"cfg.json"};
    ifs >> j;
}

UaServerCfg Configuration::GetServerConfig() {
    UaServerCfg cfg = j["UaServer"];
    return cfg;
}

ModbusTcpClientCfg Configuration::GetModbusClientConfig() {
    ModbusTcpClientCfg cfg = j["ModbusClient"];
    return cfg;
}