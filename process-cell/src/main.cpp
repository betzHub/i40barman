//
// Created by magino on 10.4.19.
//

#include <csignal>
#include <thread>

#include <Configuration/Configuration.h>
#include <Communication/ModbusTcpClient.h>
#include <Composite/S7NodeTree.h>
#include <CompositeBuilder/S7CsvCompositeBuilder.h>
#include <CompositeBuilder/ICompositeBuilder.h>

#include <open62541/plugin/log_stdout.h>
#include <ManagementNode.h>
#include "Reservation.h"

#include "main.h"

static void stopHandler(int sign) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Received ctrl-c");
    running = false;
}
// TODO refactor these callbacks
static void beforeReadTime(UA_Server *server,
                           const UA_NodeId *sessionId, void *sessionContext,
                           const UA_NodeId *nodeId, void *nodeContext,
                           const UA_NumericRange *range, const UA_DataValue *data)
                           {
    // I need to access Communication service and by nodeId get data from S7NodeTree, necessary for get right value from communication
    IS7Node* variableNode = nodeTree->FindNode(nodeId);
    if(!variableNode) return;

    UA_Variant value = communication->GetValue(variableNode->GetCommParam());
    UA_Server_writeValue(server, variableNode->GetNodeId(), value);
}

static void afterWriteTime(UA_Server *server,
                           const UA_NodeId *sessionId, void *sessionContext,
                           const UA_NodeId *nodeId, void *nodeContext,
                           const UA_NumericRange *range, const UA_DataValue *data)
                           {

    if(sessionId->namespaceIndex == 0)
        return;

    IS7Node* variableNode = nodeTree->FindNode(nodeId);
    if(!variableNode) return;

    //UA_Variant val = data->value;
    communication->WriteValue(data->value, variableNode->GetCommParam());

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "The variable was updated");
}

int main(int argc, char **argv) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Hello, from process cell project!");
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    Configuration* cfg = new Configuration();
    communication = new ModbusTcpClient(cfg->GetModbusClientConfig());
    MyUaServer* uaServer = new MyUaServer(cfg->GetServerConfig());

    int myNamespaceIndex = uaServer->AddNamespace("http://vutbr.cz/UA/barman/");
    myNamespaceIndex = 0;

    ManagementNode::CreateUaObjectType(*uaServer, myNamespaceIndex);
    Reservation::CreateUaObjectType(*uaServer, myNamespaceIndex);

    ManagementNode management(*uaServer, myNamespaceIndex);
    management.Instantiate(UaNodeId::ObjectsFolder, UaNodeId::Organizes, "Management");

    // TODO create creation logic from cfg file by communication (S7, modbus, ..), node tree builder (csv, ?, ..)
    // HARD CODED start
    ICompositeBuilder *csvBuilder = new S7CsvCompositeBuilder("ModbusData2.csv", communication, uaServer);
    IS7Node *node = csvBuilder->BuildRoot()
                              ->BuildTree()
                              ->GetRootNode();

    nodeTree = new S7NodeTree(node);
    nodeTree->BuildOpcUaAddressSpace();

    std::thread commThread(&ICommunication::StartComm, communication, &running);
    std::thread uaServerThread(&MyUaServer::Run, uaServer, &running);

    uaServer->SetServerCallbacks();

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Joining Threads");
    commThread.join();
    uaServerThread.join();
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "Threads finished");
    return 0;
}