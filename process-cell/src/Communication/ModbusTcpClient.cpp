//
// Created by magino on 20.5.2019.
//
#include <cmath>
#include <open62541/plugin/log_stdout.h>

#include "Communication/ModbusTcpClient.h"
#include "core/UaObjects.h"


ModbusTcpClient::ModbusTcpClient(ModbusTcpClientCfg cfg): config(cfg) {
    modbus = modbus_new_tcp(config.ipAddress.c_str(), config.port);
    modbus_set_slave(modbus,2);
    // Set infinite reconnect on communication fail
    modbus_set_error_recovery(modbus, MODBUS_ERROR_RECOVERY_LINK);
    // TODO get endian setting from cfg file
    endian = Endian::little_endian;
    // TODO temporary ... min/max address should be loaded from S7NodeTree
    startAddress = config.minAddress;
    endAddress = config.maxAddress;

    buffer = new uint16_t[GetAddressDiff()/2];
}

void ModbusTcpClient::Reconnect(volatile bool* running) {
    int status;
    while(*running && !isConnected) {
        status = modbus_connect(modbus);
        if(status != -1) {
            isConnected = true;
        } else std::cout << modbus_strerror(errno) << std::endl;
        sleep(1);
    }
}

void ModbusTcpClient::GetData() {
    // read data to registers
    lock.lock();
    if(modbus_read_registers(modbus, startAddress, GetAddressDiff()/2, buffer) == -1)
        std::cout << modbus_strerror(errno) << std::endl;
    lock.unlock();
}

void ModbusTcpClient::StopComm() {
    modbus_close(modbus);
    modbus_free(modbus);
}

int ModbusTcpClient::GetModbusRegisterPos(int pos) { return pos < 2 ? 0 : (pos - startAddress) / 2; }
int ModbusTcpClient::GetCycleTime() { return config.cycleTime; }

UA_Variant ModbusTcpClient::GetValue(void *params) {
    S7CsvRowModel* rowModel = static_cast<S7CsvRowModel*>(params);
    /*
    switch (rowModel->data_type) {
        case SiemensDataType::BOOL : {
            UA_Boolean boolVal = GetBoolAt(rowModel->offset);

            UA_Variant variant;
            UA_Variant_setScalarCopy(&variant, &boolVal, &(rowModel->ua_data_type));
            //UA_Variant_setScalar(&variant, &boolVal, &(rowModel->ua_data_type));
            return variant;
        }
        case SiemensDataType::UINT: {
            UA_UInt16 uintVal = GetUInt16((int)rowModel->offset);

            UA_Variant variant;
            UA_Variant_setScalarCopy(&variant, &uintVal, &(rowModel->ua_data_type));
            //UA_Variant_setScalar(&variant, &uintVal, &(rowModel->ua_data_type));
            return variant;
        }
        case SiemensDataType::UDINT: {
            UA_UInt32 udintVal = GetUInt32((int)rowModel->offset);

            UA_Variant variant;\
            UA_Variant_setScalarCopy(&variant, &udintVal, &(rowModel->ua_data_type));
            //UA_Variant_setScalar(&variant, &udintVal, &(rowModel->ua_data_type));
            return variant;
        }
    }
    */

    void* value =  UA_new(&rowModel->ua_data_type);

    switch (rowModel->data_type) {
        case SiemensDataType::BOOL : {
            UA_Boolean boolVal = GetBoolAt(rowModel->offset);
            value = &boolVal;
            break;
        }
        case SiemensDataType::BYTE: case SiemensDataType::USINT: {
            int offset = (int)rowModel->offset;
            UA_Byte byteVal = (offset%2==0) ? MODBUS_GET_HIGH_BYTE(buffer[GetModbusRegisterPos(offset)]) : MODBUS_GET_LOW_BYTE(buffer[GetModbusRegisterPos(offset)]);
            value = &byteVal;
            break;
        }
        case SiemensDataType::SINT: {
            int offset = (int)rowModel->offset;
            UA_SByte sbyteVal = (offset%2==0) ? MODBUS_GET_HIGH_BYTE(buffer[GetModbusRegisterPos(offset)]) : MODBUS_GET_LOW_BYTE(buffer[GetModbusRegisterPos(offset)]);
            value = &sbyteVal;
            break;
        }
        case SiemensDataType::INT: {
            UA_Int16 int16Val = (UA_Int16)buffer[GetModbusRegisterPos((int)rowModel->offset)];
            value = &int16Val;
            break;
        }
        case SiemensDataType::UINT: case SiemensDataType::WORD: {
            UA_UInt16 uint16Val = buffer[GetModbusRegisterPos((int)rowModel->offset)];
            value = &uint16Val;
            break;
        }
        case SiemensDataType::DINT: {
            UA_Int32 int32Val = MODBUS_GET_INT32_FROM_INT16(buffer, GetModbusRegisterPos((int)rowModel->offset));
            value = &int32Val;
            break;
        }
        case SiemensDataType::UDINT: case SiemensDataType::DWORD: {
            UA_UInt32 uInt32Val = MODBUS_GET_INT32_FROM_INT16(buffer, GetModbusRegisterPos((int)rowModel->offset));
            value = &uInt32Val;
            break;
        }
        case SiemensDataType::LINT: {
            UA_Int64 int64Val = MODBUS_GET_INT64_FROM_INT16(buffer, GetModbusRegisterPos((int)rowModel->offset));
            value = &int64Val;
            break;
        }
        case SiemensDataType::ULINT: case SiemensDataType::LWORD: {
            UA_UInt64 uInt64Val = MODBUS_GET_INT64_FROM_INT16(buffer, GetModbusRegisterPos((int)rowModel->offset));
            value = &uInt64Val;
            break;
        }
        case SiemensDataType::REAL: {
            int offset = GetModbusRegisterPos((int)rowModel->offset);
            uint16_t mirror[2];
            mirror[1]= *(buffer + offset);
            mirror[0]= *(buffer + offset+1);
            UA_Float floatVal = modbus_get_float(mirror);
            // badc
            value = &floatVal;
            break;
        }
        case SiemensDataType::LREAL: {
            //TODO Double
            UA_Double doubleVal;
            value = &doubleVal;
            break;
        }
        case SiemensDataType::CHAR: {
            int offset = (int)rowModel->offset;
            uint16_t val = buffer[GetModbusRegisterPos(offset)];
            char byteArray[2];
            byteArray[0] = (offset%2==0) ? MODBUS_GET_HIGH_BYTE(val) : MODBUS_GET_LOW_BYTE(val);
            byteArray[1] = '\0';
            UA_String charVal = UA_String_fromChars(byteArray);

            value = &charVal;
            break;
        }
        case SiemensDataType::STRING: {
            UA_String stringVal = GetString((int)rowModel->offset);
            value = &stringVal;
            break;
        }
    }

    UA_Variant variant;
    UA_Variant_setScalarCopy(&variant, value, &(rowModel->ua_data_type));
    return variant;
}

UA_Boolean ModbusTcpClient::GetBoolAt(float offset) {
    int pos = (int)offset;
    int bit = (offset - pos)*10;

    // TODO check (bit < 0 && bit > 7) && pos is in min and max address
    // TODO what if startAddress is odd -> S7 data in data block starts always from even number!
    uint16_t val = buffer[GetModbusRegisterPos(pos)];

    if(endian == Endian::little_endian && pos % 2 == 0)
        bit += 8;

    return (val & ( 1 << bit )) >> bit != 0;
}
/*
UA_Int16 ModbusClient::GetInt16(int pos) {
    return buffer[GetModbusRegisterPos(pos)];
}
UA_UInt16 ModbusClient::GetUInt16(int pos) { return buffer[GetModbusRegisterPos(pos)]; }

UA_Int32 ModbusClient::GetInt32(int pos) {
    return MODBUS_GET_INT32_FROM_INT16(buffer, GetModbusRegisterPos(pos));
}
UA_UInt32 ModbusClient::GetUInt32(int pos) { return MODBUS_GET_INT32_FROM_INT16(buffer, GetModbusRegisterPos(pos)); }

UA_Int64 ModbusClient::GetInt64(int pos) {
    return MODBUS_GET_INT64_FROM_INT16(buffer, GetModbusRegisterPos(pos));
}
UA_UInt64 ModbusClient::GetUInt64(int pos) {
    return MODBUS_GET_INT64_FROM_INT16(buffer, GetModbusRegisterPos(pos));
}
*/

UA_String ModbusTcpClient::GetString(int pos) {
    int startRegister = GetModbusRegisterPos(pos);
    uint16_t metaData = buffer[startRegister];
    int actualLength = MODBUS_GET_LOW_BYTE(metaData);
    char byteArray[actualLength+1];

    int j = 0;
    int other = GetModbusRegisterPos(pos + 2 + actualLength);
    for(int i = startRegister + 1; i <= other; ++i) {
        byteArray[j] = MODBUS_GET_HIGH_BYTE(buffer[i]);
        if( i == other && (actualLength % 2 != 0)) break;
        byteArray[j+1] = MODBUS_GET_LOW_BYTE(buffer[i]);
        j+=2;
    }
    byteArray[actualLength] = '\0';
    return UA_String_fromChars(byteArray);
}





void ModbusTcpClient::WriteValue(UA_Variant data, void* params) {
    S7CsvRowModel* rowModel = static_cast<S7CsvRowModel*>(params);

    switch (data.type->typeIndex) {
        case UA_TYPES_BOOLEAN: {
            UA_Boolean boolVal = *static_cast<UA_Boolean*>(data.data);
            WriteBoolAt(boolVal, rowModel->offset);
            return;
        }
        case UA_TYPES_BYTE: {
            UA_Byte byteVal = *static_cast<UA_Byte*>(data.data);
            WriteByteAt(byteVal, (int)rowModel->offset);
            return;
        }
        case UA_TYPES_SBYTE: {
            UA_SByte sbyteVal = *static_cast<UA_SByte*>(data.data);
            WriteSByteAt(sbyteVal, (int)rowModel->offset);
            return;
        }
        case UA_TYPES_INT16: {
            UA_Int16 int16Val = *static_cast<UA_Int16*>(data.data);
            WriteInt16At(int16Val, (int)rowModel->offset);
            return;
        }
        case UA_TYPES_UINT16: {
            UA_UInt16 uint16Val = *static_cast<UA_UInt16*>(data.data);
            WriteUInt16At(uint16Val, rowModel->offset);
            return;
        }
        case UA_TYPES_INT32: {
            UA_Int32 int32Val = *static_cast<UA_Int32*>(data.data);
            WriteInt32At(int32Val, rowModel->offset);
            return;
        }
        case UA_TYPES_UINT32: {
            UA_UInt32 uint32Val = *static_cast<UA_UInt32*>(data.data);
            WriteUInt32At(uint32Val, rowModel->offset);
            return;
        }
        case UA_TYPES_INT64: {
            UA_Int64 int64Val = *static_cast<UA_Int64*>(data.data);
            WriteInt64At(int64Val, rowModel->offset);
            return;
        }
        case UA_TYPES_UINT64: {
            UA_UInt64 uint64Val = *static_cast<UA_UInt64*>(data.data);
            WriteUInt64At(uint64Val, rowModel->offset);
            return;
        }
        case UA_TYPES_FLOAT: {
            UA_Float floatVal = *static_cast<UA_Float*>(data.data);
            WriteFloatAt(floatVal, rowModel->offset);
            return;
        }
        case UA_TYPES_DOUBLE: {
            UA_Double doubleVal = *static_cast<UA_Double*>(data.data);
            // TODO double write
            return;
        }
        case UA_TYPES_STRING: {
            UA_String stringVal = *static_cast<UA_String*>(data.data);
            WriteStringAt(stringVal, rowModel->offset);
            return;
        }
    }
}

void ModbusTcpClient::WriteBoolAt(UA_Boolean val, float offset) {
    int pos = (int)offset;
    int bit = (offset - pos)*10;

    if(endian == Endian::little_endian && pos % 2 == 0)
        bit += 8;

    lock.lock();
    // Get register
    uint16_t bool_register = buffer[GetModbusRegisterPos(pos)];

    if(val) {
        bool_register |= (1 << bit); // set bit
    } else {
        bool_register &= ~(1 << bit); // clear bit
    }

    // Write register
    int status = modbus_write_register(modbus, GetModbusRegisterPos(pos), bool_register);
    lock.unlock();

    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
    // TODO set value also in buffer ?
    //buffer[pos<2 ? 0 : (pos-startAddress)/2] = (buffer[pos<2 ? 0 : (pos-startAddress)/2] & ~(1UL << bit)) | (val << bit);
}

void ModbusTcpClient::WriteByteAt(UA_Byte val, int offset) {
    uint16_t mbReg = buffer[offset];
    mbReg = offset%2==0 ? (mbReg&0xFF00)|val : (mbReg&0x00FF)|(val<<8);
    lock.lock();
    int status = modbus_write_register(modbus, GetModbusRegisterPos(offset), mbReg);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteSByteAt(UA_SByte val, int offset) {
    uint16_t mbReg = buffer[offset];
    mbReg = offset%2==0 ? (mbReg&0xFF00)|val : (mbReg&0x00FF)|(val<<8);
    lock.lock();
    int status = modbus_write_register(modbus, GetModbusRegisterPos(offset), mbReg);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteInt16At(UA_Int16 val, int pos) {
    lock.lock();
    int status = modbus_write_register(modbus, GetModbusRegisterPos(pos), val);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteUInt16At(UA_UInt16 val, int pos) {
    lock.lock();
    int status = modbus_write_register(modbus, GetModbusRegisterPos(pos), val);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteInt32At(UA_Int32 val, int pos) {
    lock.lock();
    uint16_t tmp[2]; //= {(uint16_t)(val >> 16), (uint16_t)(val & 0x0000FFFF)};
    MODBUS_SET_INT32_TO_INT16(tmp,0,val);
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), 2, tmp);
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
    lock.unlock();
}

void ModbusTcpClient::WriteUInt32At(UA_UInt32 val, int pos) {
    lock.lock();
    uint16_t tmp[2]; // = {(uint16_t)(val >> 16), (uint16_t)(val & 0x0000FFFF)};
    MODBUS_SET_INT32_TO_INT16(tmp,0,val);
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), 2, tmp);
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
    lock.unlock();
}

void ModbusTcpClient::WriteInt64At(UA_Int64 val, int pos) {
    uint16_t tmp[4];
    MODBUS_SET_INT64_TO_INT16(tmp, 0, val);
    lock.lock();
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), 4, tmp);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteUInt64At(UA_UInt64 val, int pos) {
    uint16_t tmp[4];
    MODBUS_SET_INT64_TO_INT16(tmp, 0, val);
    lock.lock();
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), 4, tmp);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteFloatAt(UA_Float val, int pos) {
    uint16_t tmp[2];
    //modbus_set_float(val, tmp);
    //modbus_set_float_cdab(val, tmp);
    //modbus_set_float_abcd(val,tmp);
    modbus_set_float_dcba(val,tmp);
    lock.lock();
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), 2, tmp);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteDoubleAt(UA_Double val, int pos) {
    uint16_t tmp[4];
    // TODO set double
    //modbus_set_float(val, tmp);
    lock.lock();
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), 4, tmp);
    lock.unlock();
    if(status == -1) std::cout << modbus_strerror(errno) << std::endl;
}

void ModbusTcpClient::WriteStringAt(UA_String val, int pos) {
    int startRegister = GetModbusRegisterPos(pos);
    uint16_t metaData = buffer[startRegister];
    int totalLength = MODBUS_GET_HIGH_BYTE(metaData);

    // val can be longer than max string length!!
    int valLength;
    if (val.length > totalLength) {
        valLength = totalLength;
        UA_LOG_WARNING(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                       "The string you typed is too long (%zu)! Shortening to %d.", val.length, totalLength);
    } else valLength = val.length;


    int tmpLength = (int) ceil((double) valLength / 2) + 1;
    uint16_t tmp[tmpLength];

    tmp[0] = (totalLength << 8) | valLength;
    int j = 0;
    for (int i = 1; i < tmpLength; ++i) {
        if (i == tmpLength - 1 && totalLength % 2 != 0 && totalLength == valLength) {
            tmp[i] = (val.data[j] << 8) | (buffer[startRegister + tmpLength - 1] & 0x00FF);
        } else {
            tmp[i] = (val.data[j] << 8) | val.data[j + 1];
        }
        j += 2;
    }

    lock.lock();
    int status = modbus_write_registers(modbus, GetModbusRegisterPos(pos), tmpLength, tmp);
    if (status == -1) std::cout << modbus_strerror(errno) << std::endl;
    lock.unlock();
}